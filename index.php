<?php
ini_set('display_errors', 1);

if(empty(session_id())) {
  session_start();
}

require_once './application/bootstrap.php';
