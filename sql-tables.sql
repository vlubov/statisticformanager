sp_groups >
CREATE TABLE `stat`.`sp_groups` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `date_cr` TIMESTAMP NOT NULL , `date_up` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
--
sp_users >
CREATE TABLE `stat`.`sp_users` ( `id` INT NOT NULL AUTO_INCREMENT , `uid` INT NOT NULL , `name` VARCHAR(255) NOT NULL , `date_cr` TIMESTAMP NOT NULL , `date_up` DATETIME NULL , PRIMARY KEY (`id`), UNIQUE `uid` (`uid`)) ENGINE = InnoDB;
--
sp_stats >
CREATE TABLE `stat`.`sp_stats` ( `id` INT NOT NULL AUTO_INCREMENT , `clients` INT NOT NULL , `messages` INT NOT NULL , `date_w` DATE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

>> add column for assesments users
ALTER TABLE sp_stats ADD assesments INT (11) DEFAULT 0;
--
sp_group_user >
CREATE TABLE `stat`.`sp_group_user` ( `id` INT NOT NULL AUTO_INCREMENT , `group_id` INT NOT NULL , `user_id` INT NOT NULL , `date_begin` DATE NOT NULL , `date_end` DATE NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
--
sp_user_stat >
CREATE TABLE `stat`.`sp_user_stat` ( `user_id` INT NOT NULL , `stat_id` INT NOT NULL ) ENGINE = InnoDB;


SELECT sp_user_stat.user_id, sp_users.name, SUM(sp_stats.clients) AS clients, SUM(sp_stats.messages) AS messages, COUNT(sp_stats.date_w) AS days
FROM sp_stats, sp_user_stat, sp_users, sp_group_user
WHERE
sp_user_stat.user_id = sp_group_user.user_id AND
sp_user_stat.user_id = sp_users.id AND
sp_user_stat.stat_id = sp_stats.id AND
(sp_stats.date_w >= '2019-06-01' AND sp_stats.date_w <= '2019-06-11') AND
((sp_group_user.date_begin <= '2019-06-01' AND sp_group_user.date_end >= '2019-06-01') AND
 sp_group_user.date_end <= '2019-06-11' OR sp_group_user.date_end IS NULL AND
 sp_group_user.group_id = 2)
GROUP BY sp_users.name
ORDER BY clients DESC

---
SELECT sp_user_stat.user_id, sp_users.name, SUM(sp_stats.clients) AS clients, SUM(sp_stats.messages) AS messages, COUNT(sp_stats.date_w) AS days
FROM sp_stats, sp_user_stat, sp_users, sp_group_user
WHERE
sp_user_stat.user_id = sp_group_user.user_id AND
sp_user_stat.user_id = sp_users.id AND
sp_user_stat.stat_id = sp_stats.id AND
sp_group_user.group_id = 2 AND
(sp_stats.date_w >= '2019-06-01' AND sp_stats.date_w <= '2019-06-11') AND
(sp_stats.date_w BETWEEN sp_group_user.date_begin AND sp_group_user.date_end) AND
((sp_group_user.date_begin <= '2019-06-01' AND sp_group_user.date_end >= '2019-06-01') AND
 sp_group_user.date_end <= '2019-06-11' OR sp_group_user.date_end IS NULL)
GROUP BY sp_users.name
ORDER BY clients DESC
---

SELECT 
	sp_user_stat.user_id, sp_users.name, SUM(sp_stats.clients) AS clients, SUM(sp_stats.messages) AS messages, COUNT(sp_stats.date_w) AS days
FROM
	sp_stats, sp_user_stat, sp_users, sp_group_user
WHERE
	sp_user_stat.user_id = sp_group_user.user_id AND
	sp_user_stat.user_id = sp_users.id AND
	sp_user_stat.stat_id = sp_stats.id AND
	(sp_stats.date_w >= '2019-06-01' AND sp_stats.date_w <= '2019-06-11') AND
	(sp_stats.date_w BETWEEN sp_group_user.date_begin AND sp_group_user.date_end) AND
	((sp_group_user.date_begin <= '2019-06-01' AND sp_group_user.date_end >= '2019-06-01') AND
	 sp_group_user.date_end <= '2019-06-11' OR sp_group_user.date_end IS NULL AND
	sp_group_user.group_id = 2)
GROUP BY
	sp_users.name
ORDER BY
	clients DESC


======
SELECT sp_user_stat.user_id, sp_users.name, SUM(sp_stats.clients) AS clients, SUM(sp_stats.messages) AS messages, COUNT(sp_stats.date_w) AS days FROM sp_stats, sp_user_stat, sp_users, sp_group_user WHERE sp_user_stat.user_id = sp_group_user.user_id AND sp_user_stat.user_id = sp_users.id AND sp_user_stat.stat_id = sp_stats.id AND sp_group_user.group_id = 1 AND (sp_stats.date_w >= '2019-06-01' AND sp_stats.date_w <= '2019-06-11') AND ((sp_group_user.date_begin <= '2019-06-01' AND sp_group_user.date_end >= '2019-06-01') AND sp_group_user.date_end <= '2019-06-11' OR sp_group_user.date_end IS NULL) GROUP BY sp_users.name ORDER BY clients DESC
======