<?php
class Controller_Statistics extends Controller {

  public function __construct() {
    $this->view = new View();
    $this->model = new Model_Statistics();
  }

  public function action_index() {
    $data = $this->model->get_data();
    $this->view->set_title_page('Statistics page of managers');
    $this->view->generate('statistics_view.php', 'template_view.php', $data);
  }

  public function action_groups($value) {
    $data = $this->model->get_data($value);
    $this->view->set_get_value($value);
    $this->view->set_title_page('Statistics page of managers by groups');
    $this->view->generate('statistics_view.php', 'template_view.php', $data);
  }

  public function action_yesterday($value) {
    $select = $this->model->set_date('yesterday');
    $data = $this->model->get_data($value);
    $this->view->set_get_value($value);
    $this->view->set_title_page('Statistics page of managers by yesterday');
    $this->view->generate('statistics_view.php', 'template_view.php', $data);
  }

  public function action_week($value) {
    $select = $this->model->set_date('week');
    $data = $this->model->get_data($value);
    $this->view->set_get_value($value);
    $this->view->set_title_page('Statistics page of managers by 7 days');
    $this->view->generate('statistics_view.php', 'template_view.php', $data);
  }

  public function action_month($value) {
    $select = $this->model->set_date('month');
    $data = $this->model->get_data($value);
    $this->view->set_get_value($value);
    $this->view->set_title_page('Statistics page of managers by 30 days');
    $this->view->generate('statistics_view.php', 'template_view.php', $data);
  }

  public function action_period($value) {
    $select = $this->model->set_date('period');

    if (!empty($_POST['group'])) {
      $value = $_POST['group'];
    } elseif (!empty($_POST['group']) === '-1') {
      $value = 'all';
    }
    
    $data = $this->model->get_data($value);
    $this->view->set_get_value($value);
    $this->view->set_title_page('Statistics page of managers by period');
    $this->view->generate('statistics_view.php', 'template_view.php', $data);
  }

}
