<?php
class Controller_Import extends Controller {

  public function __construct() {
    $this->view = new View();
    $this->model = new Model_Import();
  }

  function action_index() {
    $data = $this->model->get_data();
    $this->view->set_title_page('Import page');
    $this->view->generate('import_view.php', 'template_view.php', $data);
  }

  function action_del($value) {
    $data = $this->model->get_data($value);
    $this->view->set_title_page('Import page');
    $this->view->generate('import_view.php', 'template_view.php', $data);
  }
}
