<?php
/**
 *
 */
class Controller_Logout extends Controller {
  private $auth;

  function __construct() {
    $this->auth = new Auth();
  }

  function action_index() {
    $this->auth->logoutUser();
  }
}
