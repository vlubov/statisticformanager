<?php
class Controller_Managers extends Controller {

  public function __construct() {
    $this->model = new Model_Managers();
    $this->view = new View();
  }

  public function action_index() {
    $data = $this->model->get_data();
    $this->view->set_title_page('Managers View');
    $this->view->generate('managers_view.php', 'template_view.php', $data);
  }

  public function action_group($value) {
    $data = $this->model->get_data($value);
    $this->view->set_title_page('Managers View' . ' ' . $value);
    $this->view->generate('managers_view.php', 'template_view.php', $data);
  }

  public function action_profile($value) {
    $data = $this->model->get_data($value);
    $this->view->set_title_page('Managers Profile View' . $value);
    $this->view->generate('profiles_view.php', 'template_view.php', $data);
  }

}
