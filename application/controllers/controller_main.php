<?php
class Controller_Main extends Controller {

    public function __construct() {
      $this->model = new Model_Main();
      $this->view = new View();
    }

    public function action_index() {
        $data = $this->model->get_data();
        $this->view->set_title_page('Main page');
        $this->view->generate('main_view.php', 'template_view.php', $data);
    }
}
