<?php
/**
 *
 */
class Controller_Auth extends Controller {

  public function __construct() {
    $this->view = new View();
    $this->model = new Model_Auth();
  }

  public function action_index() {
    $data = $this->model->get_data();
    $this->view->set_title_page('Авторизация / Введите логин и пароль');
    $this->view->generate('auth_view.php', 'template_view.php', $data);
  }
}
