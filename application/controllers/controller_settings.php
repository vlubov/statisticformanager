<?php
class Controller_Settings extends Controller {

  public function __construct() {
    $this->view = new View();
    $this->model = new Model_Settings();
  }

  public function action_index() {
    $this->view->set_title_page('Setting page');
    $data = $this->model->get_data();
    $this->view->generate('settings_view.php', 'template_view.php', $data);
  }

  public function action_clean_db_tables() {
    $this->view->set_title_page('Settings / Clean DB Tables');
    $data = $this->model->clean_db_tables();
    $this->view->generate('settings_view.php', 'template_view.php', $data);
  }

}
