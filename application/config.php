<?php
/** configuration parameters system
 *  define CONSTANTS
 */
define('AUTH_NAME', 'auth_stat');
if (!empty($_SESSION['auth'])
    && !empty($_COOKIE['trc'])
    && $_SESSION['auth'] == $_COOKIE['trc']) {
  define('AUTH', 1);
} else {
  define('AUTH', 0);
}
// Database settings & host settings:
if (!strstr($_SERVER['HTTP_HOST'], '.saas')) {
  define('DBHOST', 'localhost');
  define('DB', 'stat');
  define('DBUSER', 'root');
  define('DBPASS', '');
  define('DBCHARSET', 'utf8');
  define('PROJECT', 'stats/');
// separate - windows
  define('SEPARATOR', '\\');
} else {
  define('DBHOST', '10.4.0.9');
  define('DB', 'mstat');
  define('DBUSER', 'qcrm');
  define('DBPASS', '111111');
  define('DBCHARSET', 'utf8');
  define('PROJECT', '');
// separate - linux
  define('SEPARATOR', '/');
}
define('HOST', 'http://'. $_SERVER['HTTP_HOST'] . '/' . PROJECT);
// PATH :
// path basic:
define('BASE', str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
// folder project:
define('DIR_UPL', 'upl');
// path name const:
define('APP', 'application');
define('MODEL', 'models');
define('VIEW', 'views');
define('CORE', 'core');
define('CONTR', 'controllers');
// NM - name
define('NM_MOD', 'Model_');
define('NM_VIEW', 'views');
define('NM_CONTR', 'Controller_');
define('NM_ACT', 'action_');
// DF - default, CLS - class
define('DF_CONTR', 'Main');
define('DF_ACT', 'index');
define('DF_CLS', 'class.');
// PG - page
define('DF_PG_TITLE', 'Index');
// FL - file, EXT - extension
define('EXT_PHP', '.php');
define('EXT_TXT', '.txt');
// Text file name for register userstat
define('NAME_SPACE', 'namesen' . EXT_TXT);
define('PIN_CODE', '1047');
define('PIN_CHANGE', '2158');
