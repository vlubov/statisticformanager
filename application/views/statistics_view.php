<?php
// !empty($data['req']['sql']) ? Test::pre($data['req']['sql']) : '';
$add_link = '';
if (!empty($this->get_get_value())) {
  $add_link = $this->get_get_value();
}
?>
<br>
<div class="container">
  <h2>Статистика менеджеров по чатам и сообщениям</h2>
<hr>
</div>
<?php if (!empty($data)) { ?>
<div class="container">
<?php if (!empty($data['today'])) { ?>
<div class="row form-row">
    <div class="col-3 mt-2 text-center"><br>Сегодня: <b class="h6"><?php echo $data['today']; ?></b></div>
<form action="<?php echo BASE; ?>statistics/period" method="post">
    <div class="form-row">
      <?php if (!empty($data['groups'])) {
        echo '<div class="col">Группа:<select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="group"><option value="all" selected>Выбрать...</option>';
        // echo '<option value="notassigned">Нет в группах</option>';
        foreach ($data['groups']['name'] as $key => $value) {
          echo '<option value="' . $data['groups']['id'][$key] . '">' . $value . '</option>';
        }
        echo "</select></div>";
      }
      ?>
<!-- div class="col-6" id="sandbox-container"><br>
  <div class="input-daterange input-group" id="datepicker">
    <input type="text" class="input-sm form-control" name="start" />
    <span class="input-group-addon"> по </span>
    <input type="text" class="input-sm form-control" name="end" />
  </div>
</div -->
      <div class="col">Период: с <input type="date" name="begin" class="form-control" value="<?php echo !empty($_POST['begin']) ? $_POST['begin'] : ''; ?>"></div>
      <div class="col">по <input type="date" name="end" class="form-control" value="<?php  echo !empty($_POST['end']) ? $_POST['end'] : ''; ?>"></div>
      <div class="col"><br><button type="submit" class="btn btn-primary btn-sm mt-1">Выбрать</button></div>
    </div>
</div>
</form>
<hr>

<?php if (!empty($data['groups'])) { ?>
  <div class="container">
    <div class="row h5">Просмотр за неделю, выберите группу:</div>
  </div>
    <hr>
  <div class="container">
    <div class="row">
      <form method="post" action="<?php echo BASE . 'statistics/week'; ?>">
<?php
for ($i = 0;$i < sizeof($data['groups']['name']); $i++) {
  $buttonStyle = 'btn-outline-primary';
  if (!empty($_POST['gid']) && (int) $_POST['gid'] === $data['groups']['id'][$i]) {
    $buttonStyle = 'btn-primary';
    echo '<input name="gidid" value="' . $data['groups']['id'][$i] . '" type="hidden">';
  }

  if (!empty($_POST['gidid'])
      && (int) $_POST['gidid'] === $data['groups']['id'][$i]
      && (!empty($_POST['setDateLast'])
      || !empty($_POST['setDateNext'])))
  {
    $buttonStyle = 'btn-primary';
    echo '<input name="gidid" value="' . $data['groups']['id'][$i] . '" type="hidden">';
  }
?>
          <button class="btn <?php echo $buttonStyle; ?> btn-sm mr-4" type="submit" value="<?php echo $data['groups']['id'][$i]; ?>" name="gid">
            <?php echo $data['groups']['name'][$i]; ?>
          </button>
<?php
}
?>
|| неделя:
  <button class="btn btn-link btn-sm mr-1" type="submit" value="<?php echo $data['dates']['lastWeek']; ?>" name="setDateLast">
    &lt; предыдущая
  </button>
  <b>[<?php echo $data['dates']['dateBegin'] . ' &mdash; ' . $data['dates']['dateEnd']; ?>]</b>
<?php if (time() > strtotime($data['dates']['dateEnd'])) { ?>
  <button class="btn btn-link btn-sm mr-1" type="submit" value="<?php echo $data['dates']['nextWeek']; ?>" name="setDateNext">
    следующая &gt;
  </button>
<?php } ?>
<!-- div class="col h6"><?php Test::pre($data['dates']); ?></div -->
<div class="col">&nbsp;</div>
<input type="hidden" value="<?php echo !empty($data['dates']['dateBeginLast']) ? $data['dates']['dateBeginLast'] : ''; ?>" name="beginDateLast">
<input type="hidden" value="<?php echo !empty($data['dates']['dateEndLast']) ? $data['dates']['dateEndLast'] : ''; ?>" name="endDateLast">
<?php if (time() > strtotime($data['dates']['dateEnd'])) { ?>
<input type="hidden" value="<?php echo !empty($data['dates']['dateBeginNext']) ? $data['dates']['dateBeginNext'] : ''; ?>" name="beginDateNext">
<input type="hidden" value="<?php echo !empty($data['dates']['dateEndNext']) ? $data['dates']['dateEndNext'] : ''; ?>" name="endDateNext">
<?php } ?>

      </form>
    </div>
  </div>
  <div class="container">
    <!-- div class="row alert-info alert-link pt-2 pb-2">
      <div class="col-3">Менеджер</div>
      <div class="col">Пн</div>
      <div class="col">Вт</div>
      <div class="col">Ср</div>
      <div class="col">Чт</div>
      <div class="col">Пт</div>
      <div class="col">Сб</div>
      <div class="col">Вс</div>
    </div -->

<?php
$weekDay = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
$tableHead = '<div class="row alert-info alert-link pt-2 pb-2"><div class="col-3">Менеджер</div>';
$day = 3600 * 24;
$firstDay = strtotime($data['dates']['dateBegin']);
$arrKey = [];
// $nextDay =
for ($i = 0; $i < 7; $i++) {
  $tableHead .= '<div class="col"> <b class="btn-sm">' . date('d-m', $firstDay) . '/' . $weekDay[date("w", $firstDay)] . '</b></div>';
  $arrKey[] = date('Y-m-d', $firstDay);
  $firstDay += $day;
}

$tableHead .= '</div>';

// расбрасываем массив по ячейкам
if (!empty($data['dataTable'])) {
  $table = '';
  $k = 0;

  foreach ($data['dataTable'] as $key => $value) {
    $table .= '<div class="row';
    $table .= $k % 2 === 0 ? '">' : ' alert-success">' . "\n";
    $table .= '<div class="col-3">' . $key . '</div>';
    $dataUser = [];
// Test::pre($value);
    foreach ($value as $key => $chatMess) {
      $dataUser[date("w", strtotime($key))] = $chatMess;
    }

    for($j = 0; $j < 7; $j++) {
      if (!empty($value[$arrKey[$j]])) {
        $table .= '<div class="col text-center">' . $value[$arrKey[$j]][0] . ' | ' . $value[$arrKey[$j]][1] . '</div>';
      } else {
        $table .= '<div class="col"></div>';
      }
    }
// Test::pre($dataUser);
/**/
    $tmpSunday = '';
/*    for ($i = 0; $i < 7; $i++) {
      if (!empty($dataUser[$i])) {
        if ($i === 0) {
          $tmpSunday = '<div class="col text-center">' . $dataUser[$i][0] . ' | ' . $dataUser[$i][1] . '</div>';
        } else {
          $table .= '<div class="col text-center">' . $dataUser[$i][0] . ' | ' . $dataUser[$i][1] . '</div>';
        }
      } else {
        $table .= '<div class="col"></div>';
      }
    }
*/

    $table .= $tmpSunday . '</div>' . "\n";
    $k++;
  }
echo $tableHead . $table;
}
// Test::pre($data['dataTable'],1);
?>

  </div>
  <hr>
  <!-- div class="container"><?php if(!empty($data['weekStats'])) Test::pre($data['weekStats']); ?></div -->
<?php } ?>

<br>
<hr>
<?php if (!empty($data['days'])) { ?>
<div class="container text-center"><br>
<h4 class="text-center">Чаты</h4>
<canvas id="myChart"></canvas>
<br>
<hr>
<br>
<h4 class="text-center">Сообщения</h4>
<canvas id="myChart2"></canvas>
</div>
<?php } ?>
<script type="text/javascript">
<?php
$graph = '';
$graph2 = '';

if (!empty($data['days'])) {
  echo 'var days = ['.implode(', ', $data['days']).'];'."\n";
  $chats = [];
  $messages = [];

  foreach ($data['datas'] as $key => $value) {
    $k = 0;
    foreach ($data['users'] as $keyid => $valid) {
        $k++;
        $chats[$valid][] = !empty($value[$valid]) ? $value[$valid][0] : 0;
        $messages[$valid][] = !empty($value[$valid]) ? $value[$valid][1] : 0;
    }
  }
  $k = 0;
  $color = [
    "#00ffff", "#f5f5dc", "#0000ff", "#a52a2a", "#00ffff", "#00008b",
    "#008b8b", "#a9a9a9", "#006400", "#bdb76b", "#8b008b", "#556b2f",
    "#ff8c00", "#9932cc", "#8b0000", "#e9967a", "#9400d3", "#ff00ff",
    "#ffd700", "#008000", "#4b0082", "#f0e68c", "#add8e6", "#e0ffff",
    "#90ee90", "#d3d3d3", "#ffb6c1", "#00ff00", "#ff00ff", "#800000",
    "#000080", "#808000", "#ffa500", "#ffc0cb", "#800080", "#800080",
    "#ff0000", "#c0c0c0", "#ffff00"
  ];
  $color_add = [];
  foreach ($data['users'] as $key => $value) {
    $color_add[] = $color[mt_rand(0, (sizeof($color) - 1))];
  }
  echo "\n";
  foreach ($data['managers']['id'] as $key2 => $value2) {
    $k++;
    echo 'var chats'.$k.' = [' . implode(', ', $chats[$value2]) . '];'."\n";
    echo 'var messages'.$k.' = [' . implode(', ', $messages[$value2]) . '];'."\n";
    $graph .= ' {'."\n";
    $graph .= "   label: '{$data['managers']['name'][$key2]}',"."\n";
    $graph .= "   backgroundColor: '{$color_add[$key2]}',"."\n";
    $graph .= "   borderColor: '{$color_add[$key2]}',"."\n";
    $graph .= "   data: [".implode(', ', $chats[$value2])."],"."\n";
    $graph .= "   fill: false"."\n";
    $graph .= '  },'."\n";
    $graph2 .= ' {'."\n";
    $graph2 .= "  label: '{$data['managers']['name'][$key2]}',"."\n";
    $graph2 .= "  backgroundColor: '{$color_add[$key2]}',"."\n";
    $graph2 .= "  borderColor: '{$color_add[$key2]}',"."\n";
    $graph2 .= "  data: [".implode(', ', $messages[$value2])."],"."\n";
    $graph2 .= "  fill: false"."\n";
    $graph2 .= ' },'."\n";
  }
?>

var ctx = document.getElementById('myChart').getContext('2d');
var ctx2 = document.getElementById('myChart2').getContext('2d');
var chart = new Chart(ctx, {
  type : 'line',
  data: {
    labels: days,
    datasets: [<?php echo $graph; ?>]
  },
  options: {}
});
var chart2 = new Chart(ctx2, {
  type : 'line',
  data: {
    labels: days,
    datasets: [<?php echo $graph2; ?>]
  },
  options: {}
});
</script>
  <?php } ?>
<?php } ?>
<div class="row">


<?php if (!empty($data['groups']) && FALSE) { ?>
<div class="col">
<?php
  echo '<a href="' . BASE . 'statistics">все</a> / <a href="' . BASE . 'statistics/groups/notassigned">нет группы</a> ';
  foreach ($data['groups']['name'] as $key => $value) {
    echo ' / <a href="' . BASE . 'statistics/groups/' . $data['groups']['id'][$key] . '">' . $value . '</a>';
  }
?>
</div>
<?php } ?>
<?php if (strstr($_SERVER['REQUEST_URI'], 'groups')) { ?>
<div class="col text-right">Прошлый месяц / Прошлая неделя /
    <a href="<?php echo BASE . 'statistics/yesterday/' . $add_link; ?>">Вчера</a> /
    <a href="<?php echo BASE . 'statistics/week/' . $add_link; ?>">За 7 дней</a> /
    <a href="<?php echo BASE . 'statistics/month/' . $add_link; ?>">За 30 дней</a> / Квартал</div>
<?php } ?>
</div>
<hr>
<?php
if (!empty($data['group']['name'])) {
  /*if ($add_link === 'notassigned') {
    $
  }*/
?>
<div class="container row display-4"><div class="col-auto mr-auto">
<?php
$add = '';
if (!empty($data['req']['text'])) {
  $add = ' / ' . $data['req']['text'];
}
echo $data['group']['name'] . $add;
?></div>
<!-- div class="col-auto" id="add-top-button"><button type="submit" class="btn btn-outline-success btn-sm mt-1" id="top-button" disabled>Построить график</button></div -->
</div>
<?php
}
?>
<?php if (!empty($data['exec'])) { ?>
<script type="text/javascript">
function check() {
  if (!$('#checkall').prop('checked')) {
    $('body input:checkbox').prop('checked', false);
    $('#top-button').attr('disabled', true);
  } else {
    $('body input:checkbox:not(:checked)').prop('checked', true);
    $('#top-button').attr('disabled', false);
  }
}

function dis_button() {
  if ($(':checkbox:checked').length > 1) {
    $('#top-button').attr('disabled', false);
  } else {
    $('#top-button').attr('disabled', true);
  }
}
</script>
<div class="container">
  <div class="row alert-info alert-link pt-2 pb-2">
    <div class="col-1">#</div>
    <div class="col-3">Менеджер</div>
    <div class="col">Чатов</div>
    <div class="col">Чатов/Дней</div>
    <div class="col">Сообщений</div>
    <div class="col">Сооб./Дней</div>
    <div class="col">Рабочих дней</div>
    <!-- div class="col-1"><input id="checkall" class="form-check-input" type="checkbox" onchange="check()">
      <label for="checkall">все</label>
    </div -->
  </div>
<?php
$k = 0;
$chats = 0;
$messages = 0;
$days = 0;
// Test::pre($data['exec']);
foreach ($data['exec'] as $key => $value) {
  $add_class = '';
  $k++;
  if ($k % 2 === 0) {
    $add_class = ' alert-success';
  }
  echo '<div class="row ' . $add_class . '">';
  echo '  <div class="col-1">' . ($key + 1) . '</div>';
  echo '  <div class="col-3">' . $value['name'] . '</div>';
  echo '  <div class="col">' . $value['chats'] . '</div>';
  echo '  <div class="col">' . round($value['chats'] / $value['workdays'], 2) . '</div>';
  echo '  <div class="col">' . $value['mess'] . '</div>';
  echo '  <div class="col">' . round($value['mess'] / $value['workdays'], 2) . '</div>';
  echo '  <div class="col">' . $value['workdays'] . '</div>';
  // echo '  <div class="col-1" id="dm' . $value['uid'] . '"><input id="m' . $value['uid'] . '" class="form-check-input" type="checkbox" onchange="dis_button()"></div>';
  echo '</div>';
  $chats += (int) $value['chats'];
  $messages += (int) $value['mess'];
  $days += (int) $value['workdays'];
}
?>
  <div class="row alert-primary alert-link mt-1 pb-1 pt-1">
    <div class="col-1">Всего</div>
    <div class="col-3"><?php echo $k; ?></div>
    <div class="col"><?php echo $chats; ?></div>
    <div class="col"><?php echo round($chats / $days, 2); ?></div>
    <div class="col"><?php echo $messages; ?></div>
    <div class="col"><?php echo round($messages / $days, 2); ?></div>
    <div class="col"><?php echo $days; ?></div>
    <!-- div class="col-1" id="add-button"></div -->
  </div>
</div>
<hr>

<?php } else { ?>
<br>
<div class="container">
  <div class="alert alert-info">У данной группы за выбранный период не было сообщений</div>
</div>
<br>
<hr>
<?php } ?>

</div>
<?php } ?>
