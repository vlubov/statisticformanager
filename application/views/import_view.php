<br>
<div class="container">
  <h2>Загрузка данных</h2>
<hr>
<div class="container alert alert-light">
  Имя файла должно быть в формате  <u>ГГГГ-ММ-ДД</u> (Год-Месяц-День, пример:
  <u><?php echo date("Y-m-d", time() - mt_rand(86400, 31536000) * mt_rand(0, 5)); ?></u>).
  Система автоматически считывает имя файла и сохраняет данные, считая его указанным днем.
</div>
<br>
<?php
if (!empty($data['msg']) || !empty($data['err'])) {
  $add_class = !empty($data['msg']) ? 'alert alert-success' : 'alert alert-danger';
?>
<div class="<?php echo $add_class; ?>"><?php echo !empty($data['msg']) ? $data['msg'] : $data['err']; ?></div>
<?php
}
?>
<div class="container">
  <form action="./import" enctype="multipart/form-data" method="post">
    <input type="hidden" value="form is sending" name="hidesend">
    <div class="form-group alert alert-info row">
      <div class="col">
      <label for="FormControlFile">
        Загрузить файл статистики:
        <i>(пример названия файла:
        <b><?php echo date("Y-m-d", time() - mt_rand(86400, 31536000) * mt_rand(0, 5)); ?>.txt</b>)</i></label>
      <input type="file" class="form-control-file" id="FormControlFile" name="sendfile">
      </div>
      <div class="col-2"><br>
        <input type="submit" class="btn btn-primary mb-2" id="filesubmit" name="filesubmit" value="Загрузить">
      </div>
    </div>
  </form>
</div>

<br><hr>

<?php if(!empty($data['files'])) { ?>
<?php if(!empty($data['del_msg']) || !empty($data['del_err'])) { ?>
<?php if(!empty($data['del_err'])) { ?>
<div class="alert alert-danger"><?php echo $data['del_err']; ?></div>
<?php } ?>
<?php if(!empty($data['del_msg'])) { ?>
<div class="alert alert-success"><?php echo $data['del_msg']; ?></div>
<?php } ?>
<?php } ?>
<div class="container"><h2>Загруженные файлы:</h2>
<div class="row alert-link alert-info pt-2 pb-2">
  <div class="col-1 text-right">#</div>
  <div class="col">Имя и ссылка на файл</div>
  <div class="col text-right pr-2"><a href="/<?php echo PROJECT; ?>import/del/all">[Удалить все файлы]</a></div>
</div>
<?php
foreach ($data['files'] as $key => $value) {
  $add = (($key-1) % 2 == 0) ? 'alert-success' : '';
  echo '<div class="row pt-1 pb-1 ' . $add .
    '"><div class="col-1 text-right">' . ($key + 1) .
    '</div><div class="col">' . $value .
    '</div><div class="col-1"><a href="/' . PROJECT . 'import/del/' . rawurlencode(strip_tags($value)) .
    '">[удалить]</a></div></div>';
}
?>
</div>
<br><br><br>
<?php } ?>
</div>
