<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $this->get_title_page(); ?></title>
    <script src="<?php echo BASE; ?>js/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo BASE; ?>css/bootstrap.min.css">
    <!-- link rel="stylesheet" type="text/css" href="<?php echo BASE; ?>css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE; ?>css/bootstrap-datepicker.standalone.css" -->


    <script type="text/javascript" src="<?php echo BASE; ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
    <!-- script type="text/javascript" src="<?php echo BASE; ?>js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE; ?>locales/bootstrap-datepicker.ru.min.js" charset="UTF-8"></script -->
    <link rel="stylesheet" type="text/css" href="<?php echo BASE; ?>css/style.css">
</head>
<body>
<div class="container">
<br>
        <?php $str = explode('/',$_SERVER['REQUEST_URI']);
        if (!empty($str[1])) { ?>
          <a class="alert alert-link" href='<?php echo BASE; ?>'>Главная страница</a>
        <?php } else { ?>
          <b class="alert alert-link">Главная страница</b>
        <?php } ?> /
        <?php if (!strstr($str[1], 'statistics')) { ?>
          <a class="alert alert-link" href='<?php echo BASE; ?>statistics'>Статистика</a>
        <?php } else { ?>
          <b class="alert alert-link">Статистика</b>
        <?php } ?>

<?php if(!empty(AUTH)) { ?>
         /
        <?php if (!strstr($str[1], 'import')) { ?>
          <a class="alert alert-link" href='<?php echo BASE; ?>import'>Загрузить данные</a>
        <?php } else { ?>
          <b class="alert alert-link">Загрузить данные</b>
        <?php } ?> /
        <?php if (!strstr($str[1], 'managers')) { ?>
          <a class="alert alert-link" href='<?php echo BASE; ?>managers'>Менеджеры</a>
        <?php } else { ?>
          <b class="alert alert-link">Менеджеры</b>
        <?php } ?> /
        <?php if (!strstr($str[1], 'settings')) { ?>
          <a class="alert alert-link" href='<?php echo BASE; ?>settings'>Настройки</a>
        <?php } else { ?>
          <b class="alert alert-link">Настройки</b>
        <?php } ?> /
        <?php if (!strstr($str[1], 'logout')) { ?>
          <a class="alert alert-link" href='<?php echo BASE; ?>logout'>Выход</a>
        <?php } else { ?>
          <b class="alert alert-link">Выход</b>
        <?php } ?>
<?php } else { ?> /
        <?php if (!strstr($str[1], 'auth')) { ?>
          <a class="alert alert-link" href='<?php echo BASE; ?>auth'>Авторизация</a>
        <?php } else { ?>
          <b class="alert alert-link">Авторизация</b>
        <?php } ?>
<?php } ?>
<br>&nbsp;
</div>
<div class="container">
    <?php require_once 'application/views/' . $content_view; ?>
</div>
</body>
</html>
