<?php
if (!empty($data)) {
?>
<br>
<div class="container"><h2>Группы менеджеров</h2></div><hr><br>
<?php

if (!is_string($data['groups']) && sizeof($data['groups']) > 0) {
?>
<div class="container">
  <div class="row alert-link alert-info pt-2 pb-2">
    <div class="col-4">Группа</div>
    <div class="col">
      <a href="<?php echo BASE; ?>managers/group/all">Все группы</a> /
      <a href="<?php echo BASE; ?>managers/group/notassigned">Группа не назначена</a></div>
  </div>
<?php
  $m = 0;
  // echo '';
  foreach ($data['groups']['name'] as $key => $value) {
    $m++;
    $add_class = '';
    if (($m % 2) === 0) {
      $add_class = ' alert-secondary pt-1 pb-1';
    } else {
      $add_class = ' pt-1 pb-1';
    }
    echo '<div class="row'.$add_class.'"><div class="col"><a href="' . BASE . 'managers/group/' . $data['groups']['id'][$key] . '">'.$value.'</a></div></div>';
  }
  echo '<hr>';
  echo '</div>';
} else {
?>
  <div class="alert alert-warning">Группы еще не созданы! Для возможности распределения менеджеров по группам, нужно создать группы!</div>
<?php
}

?>
<br>
<div class="container"><h2>Менеджеры</h2>
<form action="<?php echo BASE; ?>managers" method="post">
<input type="hidden" name="groups" value="save-groups">
<?php if (!is_string($data['groups']) && sizeof($data['groups']['name']) > 0 && (!empty($data['name']) && sizeof($data['name']) > 0)) { ?>
  <div class="row alert-light mb-1">
    <div class="col-3"></div>
    <div class="col"></div>
    <div class="col"></div>
    <div class="col">Действия:</div>
    <div class="col">
      <input type="submit" value="Сохранить" class="btn btn-outline-primary btn-sm" name="save-top">
    </div>
  </div>
<?php } ?>
  <div class="row alert-link alert-info pt-2 pb-2">
    <div class="col-1">#</div>
    <div class="col-3">Менеджер</div>PZwD2ckJxUWGUh7rfE5abke2UYhLCnfremEiEr6l
<?php
// Test::pre($data);
if (!is_string($data['groups']) && sizeof($data['groups']['name']) > 0) {
  foreach ($data['groups']['name'] as $key => $value) {
    echo '<div class="col">' . $value . '</div>';
  }
}
?>

  </div>
<?php

// generate table of managers
if (!empty($data['name']) && sizeof($data['name']) > 0) {
  // Test::eh('Test',1);
  // Test::pre($data['user_group']);
  for ($i = 0; $i < sizeof($data['name']); $i++) {
  // if (!empty($data['name'][$i]))
    $add_class = '';
    if ((($i+1) % 2) === 0) {
      $add_class = ' alert-success';
    }

    if (empty($data['user_group'][$i][0]) && !strstr($_SERVER['REQUEST_URI'], 'notassigned')) {
      $add_class = ' alert-danger';
    }
    echo '  <div class="row' . $add_class . ' pt-1 pb-1">
          <div class="col-1">' . ($i + 1) . '</div>';
    echo '<div class="col-3"><a href="' . BASE . 'managers/profile/' . $data['id'][$i] . '">' . $data['name'][$i] . '</a></div>';
      if (!is_string($data['groups']) && sizeof($data['groups']) > 0) {
        $j = 1;
        foreach ($data['groups']['id'] as $key => $value) {
          $checked = '';
          if (!empty($data['user_group'][$i][0]) && (int) $data['user_group'][$i][0] === (int) $value) {
            $checked = ' checked';
          }
          // here needs check to save in table sp_group_user group id
          echo '<div class="col">' .
            "\n" .
            '          <div class="custom-control custom-radio">' .
            "\n" .
            '            <input type="radio" class="custom-control-input" id="customRadio1' . ($i + 1) . $j . '" name="radio_' . ($i + 1) . '" value="' . $value . '-'.$data['id'][$i].'"'.$checked.'>';
            if (empty($checked)) {
              echo '            <label class="custom-control-label" for="customRadio1' . ($i + 1) . $j . '"><small title="выбрать" alt="выбрать">[выбр]</small></label>';

            } else {
              echo '            <label class="custom-control-label" for="customRadio1' . ($i + 1) . $j . '"><small><b>' . $data['groups']['name'][$key] . '</b></small></label>';
            }
          echo '            </div>' . "\n" . '          </div>';
          $j++;
        }
      }

    echo '</div>'."\n";
    $add_class = '';
  }
} else {
  echo '<br><div class="alert alert-warning">У вас еще нет менеджеров. Проведите импорт на странице импорта.</div>';
}

?>
<?php if (!is_string($data['groups']) && sizeof($data['groups']['name']) > 0 && (!empty($data['name']) && sizeof($data['name']) > 0)) { ?>
  <div class="row alert alert-light">
    <div class="col-3"></div>
    <div class="col"></div>
    <div class="col"></div>
    <div class="col"></div>
    <div class="col">
      <input type="submit" value="Сохранить" class="btn btn-outline-primary btn-sm mt-0" name="save-buttom">
    </div>
  </div>
<?php } ?>
</div>
</form>
<?php
}
