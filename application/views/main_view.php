<br>
<div class="container">
  <div class="row">
    <h2 class="col">Сегодня: <?php echo date("Y-m-d"); ?></h2>
    <div class="col text-right h2">Группы в этом месяце</div>
  </div>
<hr>
</div>
<div class="container text-center"><br>
<h4 class="text-center">Чаты</h4>
<canvas id="myChart"></canvas>
<br><hr><br>
<h4 class="text-center">Сообщения</h4>
<canvas id="myChart2"></canvas>
</div>
<script type="text/javascript">
<?php
if (!empty($data['days'])) {
  echo 'var days = ['.implode(', ', $data['days']).'];'."\n";
  $chats = [];
  $messages = [];

  foreach ($data['datas'] as $key => $value) {
    $k = 0;
    foreach ($data['groups']['id'] as $keyid => $valid) {
        $k++;
        $chats[$k][] = !empty($value[$valid]) ? $value[$valid][0] : 0;
        $messages[$k][] = !empty($value[$valid]) ? $value[$valid][1] : 0;
    }
  }
  $k = 0;
  $graph = '';
  $graph2 = '';
  $color = [
    "#00ffff", "#f5f5dc", "#0000ff", "#a52a2a", "#00ffff",
    "#00008b", "#008b8b", "#a9a9a9", "#006400", "#bdb76b",
    "#8b008b", "#556b2f", "#ff8c00", "#9932cc", "#8b0000",
    "#e9967a", "#9400d3", "#ff00ff", "#ffd700", "#008000",
    "#4b0082", "#f0e68c", "#add8e6", "#e0ffff", "#90ee90",
    "#d3d3d3", "#ffb6c1", "#00ff00", "#ff00ff", "#800000",
    "#000080", "#808000", "#ffa500", "#ffc0cb", "#800080",
    "#800080", "#ff0000", "#c0c0c0", "#ffff00"
  ];
  $color_add = [];
  foreach ($data['groups']['id'] as $key => $value) {
    $color_add[] = $color[mt_rand(0, (sizeof($color) - 1))];
  }

  foreach ($data['groups']['id'] as $key2 => $value2) {
    $k++;
    echo 'var chats'.$k.' = [' . implode(', ', $chats[$value2]) . '];'."\n";
    echo 'var messages'.$k.' = [' . implode(', ', $messages[$value2]) . '];'."\n";
    $graph .= ' {'."\n";
    $graph .= "   label: {$data['groups']['name'][$key2]},"."\n";
    $graph .= "   backgroundColor: '{$color_add[$key2]}',"."\n";
    $graph .= "   borderColor: '{$color_add[$key2]}',"."\n";
    $graph .= "   data: [".implode(', ', $chats[$value2])."],"."\n";
    $graph .= "   fill: false"."\n";
    $graph .= '  },'."\n";
    $graph2 .= ' {'."\n";
    $graph2 .= "   label: {$data['groups']['name'][$key2]},"."\n";
    $graph2 .= "   backgroundColor: '{$color_add[$key2]}',"."\n";
    $graph2 .= "   borderColor: '{$color_add[$key2]}',"."\n";
    $graph2 .= "   data: [".implode(', ', $messages[$value2])."],"."\n";
    $graph2 .= "   fill: false"."\n";
    $graph2 .= '  },'."\n";
  }

  echo 'var groupname = ['.implode(', ', $data['groups']['name']).'];'."\n";
} else {
  echo 'var days = ["1", "2", "3", "4"];';
}
?>

var ctx = document.getElementById('myChart').getContext('2d');
var ctx2 = document.getElementById('myChart2').getContext('2d');
var chart = new Chart(ctx, {
  type : 'line',
  data: {
    labels: days,
    datasets: [<?php echo $graph; ?>]
  },
  options: {}
});
var chart2 = new Chart(ctx2, {
  type : 'line',
  data: {
    labels: days,
    datasets: [<?php echo $graph2; ?>]
  },
  options: {}
});
</script>
<br><hr><br><br>
