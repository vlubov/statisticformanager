<?php if(!empty($data)) {
// Test::pre($data);
?>
<div class="container">
<br>

<div class="container"><h2>Группы менеджеров</h2></div><hr><br>
<?php

if (!is_string($data['groups']) && sizeof($data['groups']) > 0) {
?>
<div class="container">
  <div class="row alert-link alert-info pt-2 pb-2">
    <div class="col-4">Группа</div>
    <div class="col">
      <a href="<?php echo BASE; ?>managers/group/all">Все группы</a> /
      <a href="<?php echo BASE; ?>managers/group/notassigned">Группа не назначена</a></div>
  </div>
<?php
  $m = 0;
  foreach ($data['groups']['name'] as $key => $value) {
    $m++;
    $add_class = '';
    if (($m % 2) === 0) {
      $add_class = ' alert-secondary pt-1 pb-1';
    } else {
      $add_class = ' pt-1 pb-1';
    }
    echo '<div class="row'.$add_class.'"><div class="col"><a href="' . BASE . 'managers/group/' . $data['groups']['id'][$key] . '">'.$value.'</a></div></div>';
  }
  echo '<hr>';
  echo '</div>';
} else {
?>
  <div class="alert alert-warning">Группы еще не созданы! Для возможности распределения менеджеров по группам, нужно создать группы!</div>
<?php } ?>
<br>
<div class="container"><h2 class="row">Профиль:

<?php
if (!empty($data['prof']['name'])) {
  echo '<b class="col text-right">'. $data['prof']['name'] . ' / ' . $data['prof']['uname']."</b>";
}
?>

</h2></div><hr><br>

<form action="<?php echo BASE; ?>managers/profile/<?php echo $data['prof']['uid']; ?>" method="post">
<input type="hidden" name="groups" value="save-groups">
  <div class="row alert-link alert-info pt-2 pb-2">
    <div class="col-1">#</div>
    <div class="col-4">Менеджер</div>
<?php
if (!is_string($data['groups']) && sizeof($data['groups']['name']) > 0) {
  foreach ($data['groups']['name'] as $key => $value) {
    echo '<div class="col">' . $value . '</div>';
  }
}
?>
    <div class="col">Действия:</div>
  </div>
<?php
// Test::pre($data);
$add_class = '';
// generate table of managers
if (!empty($data['name']) && sizeof($data['name']) > 0) {
  for ($i = 0; $i < sizeof($data['name']); $i++) {
    if (!empty($data['prof']['uid']) && $data['id'][$i] == $data['prof']['uid']) {
      echo '  <div class="row pt-1 pb-1">
          <div class="col-1">' . ($i + 1) . '</div>';
      echo '<div class="col-4"><a href="' . BASE . 'managers/profile/' . $data['id'][$i] . '">' . $data['name'][$i] . '</a></div>';

      if (!is_string($data['groups']) && sizeof($data['groups']) > 0) {
        $j = 1;
        foreach ($data['groups']['id'] as $key => $value) {
          $checked = '';
          if (!empty($data['user_group'][$i][0]) && (int) $data['user_group'][$i][0] === (int) $value) {
            $checked = ' checked';
          }
          // here needs check to save in table sp_group_user group id
          echo '<div class="col">' .
            "\n" .
            '<div class="custom-control custom-radio">' .
            "\n" .
            '<input type="radio" class="custom-control-input" id="customRadio1' . ($i + 1) . $j . '" name="radio_' . ($i + 1) . '" value="' . $value . '-'.$data['id'][$i].'"'.$checked.'>';
            if (empty($checked)) {
              echo '<label class="custom-control-label" for="customRadio1' . ($i + 1) . $j . '"><small>[выбрать]</small></label>';

            } else {
              echo '<label class="custom-control-label" for="customRadio1' . ($i + 1) . $j . '"><small><b>' . $data['groups']['name'][$key] . '</b></small></label>';
            }
          echo '</div>' . "\n" . '</div>';
          $j++;
        }
      }

    echo '<div class="col"><input type="submit" value="Сохранить" class="btn btn-outline-primary btn-sm" name="save-top"></div>';
    echo '</div>'."\n";
    $add_class = '';
    }
  }
} else {
  echo '<br><div class="alert alert-warning">Такого ID менеджера нет.</div>';
}

?>
</form><hr>
<?php
if (!empty($data['prof_all_groups']) && empty($data['prof_all_groups']['err'])) {
?>
<br><br><br>
<form action="<?php echo BASE; ?>managers/profile/<?php echo $data['prof']['uid']; ?>" method="post">
  <input type="hidden" name="groups" value="change-groups">
  <input type="hidden" name="change" value="change-date">
<div class="container">
  <h2 class="row">Даты вхождения в группы: </h2></div><hr><br>
  <div class="row alert-link alert-info pt-2 pb-2">
    <div class="col-1">#</div>
    <div class="col">Группа</div>
    <div class="col">Начало периода</div>
    <div class="col">Конец периода</div>
    <div class="col text-center">Сохранение</div>
    <div class="col text-center">Удаление</div>
  </div>

<?php
$n = 0;
foreach ($data['prof_all_groups'] as $key => $value) {
  $n++;
  $add_class = '';
  if (($n % 2) === 0) {
    $add_class = ' alert-success';
  }
  echo '<div class="row pt-2 pb-2' . $add_class . '">';
  echo '<div class="col-1">' . $n . '</div>';
  echo '<div class="col">' . $value['name'] . '</div>';
  if (sizeof($data['prof_all_groups']) > $n) {
    echo '<div class="col"><input type="date" value="' . $value['begin'] . '" class="form-control" placeholder="' . $value['begin'] . '" name="change_begin-' .  $value['sid']. '"></div>';
    echo '<div class="col"><input type="date" value="' . $value['end'] . '" class="form-control" placeholder="' . $value['end'] . '" name="change_end-' .  $value['sid']. '"></div>';
    echo '<div class="col text-center"><input type="submit" value="Сохранить период" class="btn btn-outline-success btn-sm mb-0 mt-0" name="change_save-' . ($value['sid']) . '"></div>';
    echo '<div class="col text-center"><input type="submit" value="Удалить период" class="btn btn-outline-danger btn-sm mb-0 mt-0" name="change_del-' . ($value['sid']) . '"></div>';
  } else {
    echo '<div class="col"><input type="date" value="' . $value['begin'] . '" class="form-control" placeholder="' . $value['begin'] . '" name="change_begin-' .  $value['sid']. '"></div>';
    echo '<div class="col"><input type="date" value="' . $value['end'] . '" class="form-control" placeholder="' . $value['end'] . '" name="change_end-' .  $value['sid']. '" disabled></div>';
    echo '<div class="col text-center"><input type="submit" value="Сохранить период" class="btn btn-outline-success btn-sm mb-0 mt-0" name="change_save-' . ($value['sid']) . '"></div>';
    echo '<div class="col text-center"></div>';
  }
  echo '</div>';
}
?>
    <!-- div class="row alert-link mt-1 pt-2 pb-2">
      <div class="col-1"></div>
      <div class="col"></div>
      <div class="col"></div>
      <div class="col text-right pt-1">Действия:</div>
      <div class="col text-center"><input type="submit" value="Сохранить все изменения" class="btn btn-outline-success btn-sm mb-0 mt-0" name="change_save-all-<?php echo $data['prof']['uid']; ?>"></div>
      <div class="col text-center"><input type="submit" value="Удалить все периоды" class="btn btn-outline-danger btn-sm mb-0 mt-0" name="change_del-all-<?php echo $data['prof']['uid']; ?>"></div>
    </div -->
  </div>

</form>
<br><br>
<?php
}
if (!empty($data['prof_all_mess']) && empty($data['prof_all_mess']['err'])) {
?>
<br>
<br>
<br>

  <div class="container">
    <h2 class="row">Сообщения и чаты: </h2><hr><br>
    <div class="row alert-link alert-info pt-2 pb-2">
      <div class="col-1">#</div>
      <div class="col">Чаты</div>
      <div class="col">Сообщения</div>
      <div class="col">Дата</div>
    </div>

  <?php
  $n = 0;
  foreach ($data['prof_all_mess'] as $key => $value) {
    $n++;
    $add_class = '';
    if (($n % 2) === 0) {
      $add_class = ' alert-success';
    }
    echo '<div class="row pt-2 pb-2' . $add_class . '">';
    echo '<div class="col-1">' . $n . '</div>';
    echo '<div class="col">' . $value['clients'] . '</div>';
    echo '<div class="col">' .  $value['messages']. '</div>';
    echo '<div class="col">' .  $value['date_w']. '</div>';
    echo '</div>';
  }
  ?>
      <!-- div class="row alert-link mt-1 pt-2 pb-2">
        <div class="col-1"></div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col text-right pt-1">Действия:</div>
        <div class="col text-center"><input type="submit" value="Сохранить все изменения" class="btn btn-outline-success btn-sm mb-0 mt-0" name="change_save-all-<?php echo $data['prof']['uid']; ?>"></div>
        <div class="col text-center"><input type="submit" value="Удалить все периоды" class="btn btn-outline-danger btn-sm mb-0 mt-0" name="change_del-all-<?php echo $data['prof']['uid']; ?>"></div>
      </div -->
      <br><hr>
      <br><br>
    </div>
  </div>
</div>
  <?php
    }
    ?>
    </div>
    <?php
}
?>
