<br>
<div class="container">
  <h2>Создать группу:</h2>
<hr>
<?php if (!empty($data['err']) && (!empty($data['act']) && $data['act'] === 'add-group')) { ?>
  <div class="alert alert-danger mt-3">
<?php echo $data['err']; ?>
  </div>
<?php } ?>
<?php if (!empty($data['msg']) && (!empty($data['act']) && $data['act'] === 'add-group')) { ?>
<div class="alert alert-success mt-3">
<?php echo $data['msg']; ?>
</div>
<?php } ?>
  <br>
  <form action="./settings" method="post">
    <input type="hidden" name="act_group" value="add-group">
    <div class="row">
      <div class="col-9">
        <input class="form-control" type="text" placeholder="Введите название группы" name="add_name">
      </div>
      <div class="col-3">
        <button type="submit" name="add" class="btn btn-primary">Создать группу</button>
      </div>
    </div>
  </form>
  <br>
<?php if (!empty($data['groups']) && is_array($data['groups'])) { ?>
  <hr>
  <h2>Редактировать группы:</h2><br>
  <form action="./settings" method="post">
    <input type="hidden" name="act_group" value="edit-group">

    <?php if (!empty($data['err']) && (!empty($data['act']) && ($data['act'] === 'edit-group' || $data['act'] === 'save' ))) { ?>
    <div class="alert alert-danger mt-3">
    <?php echo $data['err']; ?>
    </div>
    <?php } ?>
    <?php if (!empty($data['msg']) && (!empty($data['act']) && ($data['act'] === 'edit-group' || $data['act'] === 'save' ))) { ?>
    <div class="alert alert-success mt-3">
    <?php echo $data['msg']; ?>
    </div>
    <?php } ?>
<?php
// Test::pre($data);
?>
<div class="container">
<div class="row alert alert-link alert-info pt-2 pb-2 mb-0">
  <div class="col-1">#</div>
  <div class="col-9">Редактировать название группы</div>
  <div class="col-1">Действие</div>
</div>
<?php
// if () {
  for ($i = 0; $i < sizeof($data['groups']['name']); $i++) {
    echo '<div class="row mt-2 mb-2"><div class="col-1">' .
    ($i + 1) .
    '</div><div class="col-9"><input class="form-control" type="text" placeholder="' .
    $data['groups']['name'][$i] .
    '" value="' .
    $data['groups']['name'][$i] .
    '" name="edit_name-' .
    $i .
    '"></div><div class="col-1"><button type="submit" name="del-' .
    $i .
    '" value="' .
    $data['groups']['id'][$i] .
    '" class="btn btn-outline-danger">Удалить</button></div></div>'."\n";
  }
// }
?>

    <div class="row">
      <div class="col"><br>
        <button type="submit" name="save" class="btn btn-primary">Сохранить изменения</button>
      </div>
    </div>
</div>
  </form>

<?php if (!empty($data['db_tables'])) { ?>

  <br><hr><br>
  <div class="container">
    <div class="row">
      <div class="col h2 mt-2">Таблицы базы данных:</div>
      <!-- div class="alert alert-link alert-danger mt-0 mb-0 pb-0"><a href="/<?php echo PROJECT; ?>settings/clean_db_tables">[очистить таблицы]</a></div -->

    </div>
    <hr>
<?php
  foreach ($data['db_tables'] as $key => $value) {
    echo '<div class="rom">';
    echo '  <div class="col">' . $value . '</div>';
    echo '</div>';
  }
?>
    <div class="row alert alert-danger pt-1 pb-1 mt-4">
      <div class="col-4 text-right">
        <form method="post" action="/<?php echo PROJECT; ?>settings/clean_db_tables"><button class="btn btn-outline-danger btn-sm">[очистить все таблицы в БД]</button></form>
      </div>
      <div class="col pt-1">Внимание! Действие по удалению данных не обратимо!</div>
    </div>
  </div>

<?php } ?>

  <br><hr><br><br>
</div>
<?php
}
