<?php
/**
 *
 */
class Fileman {
  public $files_arr;

  function __construct() {
    if (defined('DIR_UPL')) {
      $this->files_arr = $this->go_dir(DIR_UPL);
    } else {
      die ('Config file not exist!');
    }
  }

  public function go_dir($name) {
    $result = ['Some trouble. Check code.'];

    if (is_dir($name)) {
      $result = scandir($name);
    }

    return $result;
  }

  public function view_list_files() {
    $result = [];
    $temp = [];
    empty($this->files_arr) ? $this->__construct() : '';

    if (is_array($this->files_arr)) {
      $result = array_slice($this->files_arr, 2);
      foreach ($result as $key => $value) {
        if (!is_dir(DIR_UPL . '/' . $value)) {
          $temp[] = $value;
        }
      }
      $all = $result;
      unset($result);
      $result['all_names'] = $all;
      $result['file_names'] = $temp;
      $result['files'] = $this->link_name($temp, DIR_UPL);
    }

    return $result;
  }

  public function link_name(array $names, $path) {
    $result = [];
    foreach ($names as $key => $value) {
      $result[] = '<a href="/' . $path . '/' . $value . '">' . $value . '</a>';
    }

    return $result;
  }

}
