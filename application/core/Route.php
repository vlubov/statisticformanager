<?php
class Route {

  public static function start() {
    $controllerName = DF_CONTR;
    $actionName = DF_ACT;
    $value = 0;

		if(defined('PROJECT')) {
			$_SERVER['REQUEST_URI'] = str_replace(PROJECT, "", $_SERVER['REQUEST_URI']);
		}

    $routes = explode('/', $_SERVER['REQUEST_URI']);

    if (!empty($routes[1])) {
      $controllerName = $routes[1];
    }

    if (!empty($routes[2])) {
      $actionName = $routes[2];
    }

    if (!empty($routes[3])) {
      $value = $routes[3];
    }
/*
    if (!empty($routes[4])) {
      $managers_id = $routes[4];
    }*/

    $modelName = NM_MOD . $controllerName;
    $controllerName = NM_CONTR . $controllerName;
    $actionName = NM_ACT . $actionName;
    $modelFile = strtolower($modelName) . EXT_PHP;
    $modelPath = APP . "/" . MODEL . "/" . $modelFile;

    if (file_exists($modelPath)) {
      require_once $modelPath;
    }

    $controllerFile = strtolower($controllerName) . EXT_PHP;
    $controllerPath = APP . "/" . CONTR . "/" . $controllerFile;
    // костыль для nginx, когда 404 идет не на наш index.php - иначе выполняется то, что было без условия флага
    // $contr_flag = FALSE;

    if (file_exists($controllerPath)) {
      require_once $controllerPath;
      // $contr_flag = TRUE;
    } else {
      Route::reloadPage();

//      $controllerName = Route::dog_nail($controllerName); // dog_nail -1
      /*
      // костыль при неверной отработки Location : 404
      $controllerName = NM_CONTR . '404';
      $controllerFile = strtolower($controllerName) . EXT_PHP;
      $controllerPath = APP . "/" . CONTR . "/" . $controllerFile;
      require_once $controllerPath;
      */
    }

    // if ($contr_flag) {
      $controller = new $controllerName;
      $action = $actionName;

      if (method_exists($controller, $action)) {
        $controller->$action($value);
      } else {
        Route::reloadPage();
        /* dog_nail -2
        $controllerName = Route::dog_nail($controllerName);
        $actionName = DF_ACT;
        $action = NM_ACT . $actionName;
        $controller = new $controllerName;
        $controller->$action($value);
        */
      }

  }

/* create new function for reload page
  public function error_page_404() {
    // $host = 'http://'. $_SERVER['HTTP_HOST'] . '/' . PROJECT;
    //      header('HTTP/1.1 404 Not Found');
    // header('Status: 404 Not Found'); - comment for work in Nginx
    // header('Location: ' . $host . '404');
    //      header('Location: ' . HOST . '404');
    Route::reloadPage();
  }
*/

  public function reloadPage($code = '404', $path = '404') {
    switch ($code) {
      case '302':
        header('HTTP/1.1 302 Found');
        break;

      default:
        header('HTTP/1.1 404 Not Found');
        break;
    }

    header('Location: ' . HOST . $path);
  }

/* dog-nail for some http redirect, when does't working redirect
  public function dog_nail($controllerName) {
  // костыль при неверной отработки Location : 404
    if (!strstr($controllerName, '404')) {
      $controllerName = NM_CONTR . '404';
      $controllerFile = strtolower($controllerName) . EXT_PHP;
      $controllerPath = APP . "/" . CONTR . "/" . $controllerFile;
      require_once $controllerPath;
    }

    return $controllerName;
  }
*/
}
