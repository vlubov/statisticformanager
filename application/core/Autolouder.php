<?php
class Autoloader {
    public static function register() {
        spl_autoload_register(function ($class) {
            $dirs = [
                CORE => '',
                MODEL => DF_CLS,
                ];

            foreach ($dirs as $folder => $add_file) {
                $file = APP . SEPARATOR . $folder . SEPARATOR . $add_file .
                    str_replace(SEPARATOR, DIRECTORY_SEPARATOR, $class).
                    EXT_PHP;

                if (file_exists($file)) {
                    require_once($file);
                    break;
                }
            }
        });
    }
}
Autoloader::register();
