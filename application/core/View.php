<?php
class View {

  public $page_title = DF_PG_TITLE;
  public $get_value;
  public $public_page;

  public function __construct() {
    $this->set_pages(['main', 'statistics', 'auth', '404']);
  }

  public function set_pages($pages) {
    foreach ($pages as $value) {
      $this->public_page[] = $value . '_view.php';
    }
  }

  public function get_pages() {
    return $this->public_page;
  }

  public function generate($content_view, $template_view, $data = NULL) {
    if (is_array($data)) {
      extract($data);
    }

    if (!defined('AUTH') || empty(AUTH)) {
      $auth = FALSE;
      $this->public_page[] = 'myreq.php';

      foreach ($this->public_page as $value) {
        if ($value == $content_view) {
          $auth = TRUE;
          break;
        }
      }

      if (!$auth) {
        Route::reloadPage('302', 'auth');
      }
    }

    require_once APP . '/' . VIEW . '/' . $template_view;
  }

  /**set page title
   * @param $newtitle
   */
  public function set_title_page($newtitle) {
    $this->page_title = $newtitle;
  }

  /**get page title
   * @return string
   */
  public function get_title_page() {
    return $this->page_title;
  }

  /**set page title
   * @param $newtitle
   */
  public function set_get_value($value) {
    if ($value !== 'all' && $value !== 'notassigned') {
      Database::get_int($value);
    }
    $this->get_value = $value;
  }

  /**get page title
   * @return string
   */
  public function get_get_value() {
    return $this->get_value;
  }
}
