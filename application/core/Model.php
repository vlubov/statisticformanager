<?php
class Model {

  public function get_data() {}

  public function valid_value($value, $valid_arr, array $add = NULL) {
    $result = NULL;

    if (!empty($add) && is_array($add)) {
      foreach ($add as $add_val) {
        $valid_arr[] = $add_val;
      }
    }

    foreach ($valid_arr as $val) {
      if ($val == $value) {
        $result = $val;
      }
    }

    return !empty($result) ? $result : 'all';
  }
}
