<?php
/**
 * Class for connect and operation with Database
 */
class Database {

  private $db_pdo;

  public function __construct() {
    $this->set_connect();
  }

  public function get_db() {
    return $this->db_pdo;
  }

  public function set_connect() {
    $dsn = "mysql:host=".DBHOST.";dbname=".DB.";charset=".DBCHARSET;
    $opt = [
      PDO::ATTR_ERRMODE             =>  PDO::ERRMODE_EXCEPTION,
      PDO::ATTR_DEFAULT_FETCH_MODE  =>  PDO::FETCH_ASSOC,
      PDO::ATTR_EMULATE_PREPARES    =>  FALSE,
    ];
    try {
      $pdo = new PDO($dsn, DBUSER, DBPASS, $opt);
    } catch (PDOException $e) {
      $pdo = NULL;
      die('Подключение не удалось: ' . $e->getMessage());
    }

    $this->db_pdo = $pdo;
  }

  public function check() {
      if (!empty($this->db_pdo)) {
        $result = TRUE;
      }
      return $result ? $result : FALSE;
  }

  public function request($req) {
    if ($this->check()) {
      $result = $this->db_pdo->query($req);
    }
    return !empty($result) ? $result : NULL;
  }

  public function show_tables() {
    if ($this->check()) {
      $db_res = $this->request('SHOW TABLES');
      $result = $this->get_result($db_res);
    }

    return !empty($result) ? $result : NULL;
  }

  public function get_request($req) {
    if ($this->check()) {
      $db_res = $this->request($req);
      $result = $this->get_result($db_res);
    }

    return !empty($result) ? $result : NULL;
  }

  public function get_result($dbres) {
    $result = [];
    while ($row = $dbres->fetchcolumn()) {
      $result[] = $row;
    }
    return $result;
  }

  public function get_row_request($req) {
    if ($this->check()) {
      $db_res = $this->request($req);
      $result = $this->get_row($db_res);
    }

    return !empty($result) ? $result : NULL;
  }

  public function get_row($dbres) {
    $result = [];
    while ($row = $dbres->fetch()) {
      $result[] = $row;
    }
    return $result;
  }

  public function send_arr_request($arr_request) {
    if ($this->check() && is_array($arr_request)) {
      foreach ($arr_request as $key => $value) {
        $db_res = $this->request($value);
      }
    }
  }

  public function get_id_name_by_table($table) {
    $result = [];
    $group = $this->get_row_request('SELECT id, name FROM ' . $table);

    if (sizeof($group) > 0) {
      foreach ($group as $key => $value) {
        $result['name'][] = $value['name'];
        $result['id'][] = $value['id'];
      }
    } else {
      $result['err'] = 'Empty, No Data';
    }

    return $result;
  }

  public function valid_data($value) {
    if (is_string($value) && $value !== 'NOW()') {
      $value = "'$value'";
    }
    if (is_null($value)) {
      $value = 'NULL';
    }
    return $value;
  }

  /**
  * $table
  *
  * array $column_value = [$column => $value]
  */
  public function insert($table, array $column_value) {
    $result = [];
    if (!empty($table) && !empty($column_value) && is_array($column_value)) {
      $column = '';
      $values = '';
      $i = 0;
      foreach ($column_value as $key => $value) {
        $i++;
        if ($i < sizeof($column_value)) {
          $column .= $key . ', ';
          $values .= $this->valid_data($value) . ', ';
        } else {
          $column .= $key;
          $values .= $this->valid_data($value);
        }
      }
      $sql = 'INSERT INTO ' . $table . ' (' . $column . ') VALUES (' . $values . ')';
      $result['sql'] = $sql;
      $result['exec'] = $this->request($sql);
      $result['msg'] = !empty($result['exec']) ? 'Группа успешно добавлена' : '';
    } else {
      $result['err'] = 'Input data is incorrect!';
    }

    return $result;
  }

  /**
  * $table
  *
  * $key_del is [column => ID] or [column => value]
  */
  public function delete($table, array $key_del, $param = NULL) {
    if (!empty($table) && !empty($key_del) && is_array($key_del)) {
      $base_sql = '';
      $i = 0;
      foreach ($key_del as $key => $value) {
        $i++;
        if ($i < sizeof($key_del) && !empty($param)) {
          $base_sql .= $key . ' = ' . $this->valid_data($value) . ' ' . $param . ' ';
        } else {
          $base_sql .= $key . ' = ' . $this->valid_data($value);
          break;
        }
      }
      $result['sql'] = 'DELETE FROM ' . $table . ' WHERE ' . $base_sql;
      $result['exec'] = $this->request($result['sql']);
      $result['msg'] = !empty($result['exec']) ? 'Удаление успешно завершено' : '';
    } else {
      $result['err'] = 'Your data is incorrect!';
    }

    return $result;
  }

  public function update($table, array $column_value) {
    if (!empty($table) && !empty($column_value) && is_array($column_value)) {
      $base_id = [];
      $sql = [];

        foreach ($column_value as $key => $value) {
          if (is_array($value) && !is_string($value)) {
            foreach ($value as $column => $value_upd) {
              $base_id[$key][] = $column . ' = ' . $this->valid_data($value_upd);
            }
          }
        }

        foreach ($base_id as $key => $value) {
          if (sizeof($value) > 2) {
            $addsql = '';
            for ($i = 1; $i < sizeof($value); $i++) {
              if ($i < sizeof($value) - 1) {
                $addsql .= $value[$i] . ', ';
              } else {
                $addsql .= $value[$i];
              }
            }
            $sql[] = 'UPDATE ' . $table . ' SET ' . $addsql . ' WHERE ' . $value[0];
          } else {
            $sql[] = 'UPDATE ' . $table . ' SET ' . $value[1] . ' WHERE ' . $value[0];
          }
        }

      $result['sql'] = '';
      $result['exec'] = [];

        foreach ($sql as $key => $value) {
          $result['sql'] .= $key . ': ' . $value . ', ';
          $result['exec'][] = $this->request($value);
          $result['msg'] = !empty($result['exec'][$key]) ? ($key + 1) . ': ' . 'Данные успешно обновлены ' : 'Произошла ошибка';
        }
    } else {
      $result['err'] = 'Your data is incorrect!';
    }

    return $result;
  }

  public function get_int($data) {
    $data = preg_replace("/[^0-9]/ui", '', $data);
    return trim($data);
  }

  public function getChars($data) {
    $data = preg_replace("/[^a-zA-Z]/ui", '', $data);
    return trim($data);
  }

  public function getIntChars($data) {
    $data = preg_replace("/[^a-zA-Z0-9\-\_]/ui", '', $data);
    return trim($data);
  }

  public function show_managers($group_id = NULL, $now = NULL) {
    $result = [];
    $sql = 'SELECT id, name FROM sp_users';

    if ($group_id === 'notassigned') {
      $sql = 'SELECT id, name FROM sp_users WHERE id ' .
        'NOT IN (SELECT sp_users.id FROM sp_users, sp_group_user WHERE sp_users.id = sp_group_user.user_id AND sp_group_user.date_end IS NULL GROUP BY sp_users.id)';
    } elseif (!empty($this->get_int($group_id)) && empty($now)) {
      $sql = 'SELECT sp_users.id, sp_users.name FROM sp_users, sp_group_user WHERE ' .
        'sp_users.id = sp_group_user.user_id AND sp_group_user.date_end IS NULL AND sp_group_user.group_id = ' . $group_id .
        ' ORDER BY sp_users.name';
    }

    if (empty($group_id) && !empty($now)) {
      $sql = 'SELECT sp_users.id, sp_users.name FROM sp_users, sp_group_user WHERE ' .
        'sp_users.id = sp_group_user.user_id AND sp_group_user.group_id = ' . $group_id .
        ' ORDER BY sp_users.name';
    }

    $user_list = $this->get_row_request($sql);

    if (!empty($user_list)) {
      foreach ($user_list as $key => $value) {
        $result['name'][] = $value['name'];
        $result['id'][] = $value['id'];
      }
      $user_list = $result;
    } else {
      $user_list = NULL;
    }

    return $user_list;
  }

  public function check_user($value, $no_all_group = NULL) {
    $value = $this->get_int($value);
    if (!empty($value)) {
      $add_sql = '';
      if (!empty($no_all_group)) {
        $add_sql = 'AND sp_group_user.date_end IS NULL ';
      }

      $sql = 'SELECT sp_groups.id, sp_groups.name, sp_users.id AS uid, sp_users.name AS uname, sp_group_user.date_begin AS begin, sp_group_user.date_end AS end, sp_group_user.id AS sid ' .
        'FROM sp_groups, sp_group_user, sp_users WHERE ' .
        'sp_group_user.user_id = sp_users.id ' .
        'AND sp_groups.id = sp_group_user.group_id ' .
        // $add_sql .
        'AND sp_group_user.user_id = ' .
        $value .
        ' ORDER BY sid';

      $result = $this->get_row_request($sql);

    } else {
      $result['err'] = 'Такого менеджера не существует!';
    }

    if (empty($result)) {
      $result['err'] = 'Такого ID не существует!';
    }

    return $result;
  }

  /**
  * $sql_array => 'columns' => 'name columns', etc. => 'table', 'where', 'order', 'group'
  *
  *
  *
  */
  public function select(array $sql_array) {
    $sql = '';
    $result = [];

    if (is_array($sql_array)) {
      $sql = 'SELECT ' .
            $sql_array['columns'] .
            ' FROM ' .
            $sql_array['table'] .
            ' WHERE ' .
            $sql_array['where'] .
            ' ORDER BY ' .
            $sql_array['order'];
      $result = $this->get_row_request($sql);
    } else {
      $result['err'] = 'Такого менеджера не существует!';
    }

    if (empty($result)) {
      $result['err'] = 'Такого ID не существует!';
    }

    return $result;
  }

  public function to_charset($value) {
    $coding = [
      'CP1251',
      'UTF-8',
      'ASCII'
    ];
    $code = mb_detect_encoding($value, 'auto', TRUE);

    if ($code !== 'UTF-8') {
      $value = mb_convert_encoding($value, 'UTF-8', 'CP1251');
      }

    return $value;
  }

}
