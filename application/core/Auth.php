<?php
/**
 * base class for Auth users and register new
 */
class Auth {
  private $db;

  public function __construct() {
    $this->db = new Database();
  }

  public function checkAuth() {
    // проверяем наличие uid в куке,
    // забираем из куки уид и делаем выборку из бд по этому уид
    // если уид найден - получаем логин и девайс и сверяем с текущим
    // если все совпало - даем доступ на защищенные страницы
    // если совпадений нет - обнуляем куки и кидаем на страницу авторизации
  }

  public function authUser() {
    $user = $this->db->getIntChars($_POST['log']);
    $req = $this->actUserDb($user, 'auth');
    $req['result'] = false;

    if (!empty($req['req'][0]) && !empty($req['req'][0]['pass'])) {
      $user = $req['req'][0];
      if ($this->verifPass($_POST['pas'], $user['pass'])) {
        // get passport for view protect pages
        $req['result'] = true;
      }
    }

    if ($req['result']) {
      $this->addPasport($_POST['log']);
      Route::reloadPage(302, 'main');
    }

    return $req;
  }

  public function logoutUser() {
    // to logout - unset all data ($_COOKIE, $_SESSION) and reload page
    $this->addPasport("", -1);
    Route::reloadPage(302, 'main');
  }

  public function addPasport($login = null, $daysValid = null) {
    if (!empty($login)) {
      $_SESSION['name'] = $login;
      $_SESSION['auth'] = $this->getDataUser('trc');
      $this->setCook('uid', $this->getDataUser('uid', $login), $daysValid);
      $this->setCook('uin', $this->getDataUser('uin', $login), $daysValid);
      $this->setCook('trc', $_SESSION['auth'], $daysValid);
    }

    if ($daysValid === -1) {
      unset($_SESSION['auth']);
      $this->setCook('uid', "", $daysValid);
      $this->setCook('uin', "", $daysValid);
      $this->setCook('trc', "", $daysValid);
    }
  }

  public function genName($base = NAME_SPACE) {
    $name = 'login' . mt_rand(1, 1000);

    if (is_file($base)) {
      $file = file($base);
      if (sizeof($file) > 0) {
        $name = mt_rand(0, sizeof($file));
      }
    }

    return strtolower($file[$name]);
  }

  public function generateMask() {
    $maskarade = [
          'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz_-1234567890',
          'ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz-1234567890',
          '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz-',
          '1234567890_abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ',
          'abcdefghijklmnopqrstuvwxyz-1234567890_ABCDEFGHIJKLMNOPQRSTUVWXYZ',
          'abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ-1234567890'
    ];

    return $maskarade[rand(0, 5)];
  }

  public function genPass() {
    $abc = $this->generateMask();
    $pas = mt_rand(100000, 1000000);
    $str = mb_substr(sha1($pas.$abc), mt_rand(0, 38), 2) . mb_substr(md5($pas), mt_rand(0, 31), 1);
    $pas = '';
    $max = strlen($abc) - 1;

    for ($i = 0; $i < 6; $i++) {
      $abc = $this->generateMask();
      $pos = mt_rand(0, $max);
      $i == 0
        ? $pas = mb_substr($abc, $pos, 1)
        : $pas .= mb_substr($abc, $pos, 1);
    }

    return $pas.$str;
  }

  public function hashPass($pass) {
    return password_hash($pass, PASSWORD_BCRYPT);
  }

  public function verifPass($pass, $hash) {
    return password_verify($pass, $hash);
  }

  public function createUser($data) {
    if (!empty($data['crtUsr'])
        && $data['crtUsr'] == 'create'
        && !empty($data['pin'])
        && $data['pin'] == PIN_CODE
        && !empty($data['log'])
        && !empty($data['pas']))
    {
      $result['msg'] = 'Пользователь может быть добавлен';

      $result['sid'] = $this->getDataUser();
      $result['trace'] = $_POST['ssid'] = $this->getDataUser('trace'); // add trace to ssid in db
      $result['uip'] = $_POST['uip'] = $this->getDataUser('uip');
      $result['uid'] = $_POST['uid'] = $this->getDataUser('uid', $data['log']); // add uid to uid in db
      $result['uin'] = $this->getDataUser('uin', $data['log']); // uin
      $result['trc'] = $this->getDataUser('trc');

      // for add cookie user to auth
      $this->addPasport($data['log']);
/*
      $this->setCook('uid', $result['uid']);
      $this->setCook('uin', $result['uin']);
      $this->setCook('trc', $result['trc']);
*/
      // добавление пользователя в базу:
      $result = $this->actUserDb($data['log']);
      if (empty($result['req'])) {
        $result = $this->actUserDb($data['log'],'add');
      } else {
        $result['err'] = 'Такой пользователь уже существует!';
      }
    } else {
      $result['err'] = 'Недостаточно данных для создания пользователя.';
    }

    return $result;
  }

  public function getDataUser($need = null, $log = null, $pas = null) {
    $ip = $_SERVER['REMOTE_ADDR'];
    $result = false;

    switch ($need) {
      case 'trace':
        $result = sha1($this->getDataUser());
        break;

      case 'uip':
        $result = !empty($_SERVER['HTTP_USER_AGENT']) ? sha1($_SERVER['HTTP_USER_AGENT']) : sha1($ip);
        break;

      case 'trc':
        $uip = $this->getDataUser('uip');
        $result = substr($this->db->get_int(sha1($uip . $ip)), 0, 9)
                  . '-'
                  . $this->db->getChars($uip . $ip);
        break;

      case 'uid':
        if ($log) {
          $result = (int) substr($this->db->get_int(sha1($this->getDataUser('trace') . $log)), 0, 9);
        }
        break;

      case 'uin':
        if ($log) {
          $result = $this->db->getChars(sha1($log . $this->getDataUser('trace')));
        }
        break;

      default: //sid
        if (empty(session_id()) && session_start()) {
          $result = session_id();
        }
        break;
    }

    return $result;
  }

  public function setCook($name, $value, $days = 30) {
    $days = !empty($days) ? (int) $days : 30;
    $add = $days >= 0 ? '+' : '-';
    $value = $days < 0 ? '' : $value;
    return setcookie($name, $value, strtotime($add . $days . ' days'));
  }

/*
  public function checkUser($userName) {
    $result = false;

    if (!empty($userName)) {
      $user = $this->db->getIntChars($userName);
      $sql = 'SELECT * FROM sp_managers WHERE name = \''.$user.'\'';
      $result = $this->db->get_request($sql);
    }

    return $result;
  }
*/

  public function actUserDb($userName, $action = null) {
    $result['req'] = false;
    $sql = '';

    if (!empty($userName)) {
      $user = $this->db->getIntChars($userName);
      // action : add | del | upd | info(check)
      // DB:
      // id |	uid	name | date_cr | date_up | pass | last_pass | ssid | device
      switch ($action) {
        case 'add':
            $uid = $_POST['uid'];
            $sql = 'INSERT INTO sp_managers (uid, name, date_up, pass, last_pass, ssid, device) VALUES ('
                    . $uid . ', '
                    . '\'' . $_POST['login'] . '\',
                      NULL, '
                    . '\'' . $this->hashPass($_POST['pass']) . '\',
                      NULL, '
                    . '\'' . $_POST['ssid'] . '\', '
                    . '\'' . $_POST['uip'] . '\''
                    . ')';
            $result['req'] = $this->db->request($sql);
            if (!empty($result['req'])) {
              $result['msg'] = 'Пользователь добавлен.';
            }
          break;

        case 'auth':
            $sql = 'SELECT * FROM sp_managers WHERE name = \''.$user.'\'';
            $result['req'] = $this->db->get_row_request($sql);
          break;

        case 'chk':
          // code...
          break;

        case 'del':
          // code...
          break;

        case 'upd':
          // code...
          break;

        default:
            $sql = 'SELECT * FROM sp_managers WHERE name = \''.$user.'\'';
            $result['req'] = $this->db->get_request($sql);
          break;
      }
      $result['sql'] = $sql;
    }

    return $result;
  }

/* collapse to one function - actUserDb (action user DB)
  public function userInfo() {
    // code...
  }

  public function userDel($uid) {
    // code...
  }

  public function userUpd($uid) {
    // code...
  }
*/
}
