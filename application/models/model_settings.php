<?php
/**
 *
 */
class Model_Settings extends Model {
  private $db;

  public function __construct() {
    $this->view = new View();
    $this->db = new Database();
    $this->db_pdo = $this->db->get_db();
  }

  public function get_data() {
    $result['groups'] = empty($this->get_list_group()['err']) ? $this->get_list_group() : '';
    if (!empty($_POST['act_group'])) {
      if(!isset($_POST['save'])) {
        $result['act'] = $this->post_control($_POST['act_group']);
      } else {
        $result['act'] = 'save';
      }

      if ($result['act'] === 'add-group') {
        $result['add'] = $this->post_control($_POST['add_name']);
        $sql = $this->add_group($result['add']);
      }

      if ($result['act'] === 'edit-group') {
        $result['del'] = $this->post_id('del-')[0];
        $sql = $this->del_group($result['del']);
      }

      if (isset($_POST['save'])) {
        $result['post'] = $this->post_id('edit_name');
        $result['upd_val'] = array_diff($result['post'], $result['groups']['name']);
        $result['upd_id'] = array_intersect_key($result['groups']['id'], $result['upd_val']);
        foreach ($result['upd_id'] as $key => $value) {
          $result['upd'][] = [
            $value,
            $result['upd_val'][$key],
          ];
        }
        unset($result['upd_val']);
        unset($result['upd_id']);
        if (!empty($result['upd'])) {
          $sql = $this->upd_group($result['upd']);
        }
      }

      $result = array_merge($sql, $result);
    }

    $result['groups'] = empty($this->get_list_group()['err']) ? $this->get_list_group() : '';
    $result['db_tables'] = $this->db->show_tables();

    return $result;
  }

  public function clean_db_tables() {
    $result = ['err' => 'Таблицы не очищены'];
    $tables = $this->db->show_tables();
    foreach ($tables as $key => $value) {
      $result[] = $this->db->get_row_request('TRUNCATE ' . $value);
    }
    return $result;
  }

  public function get_list_group() {
    return $this->db->get_id_name_by_table('sp_groups');
  }

  public function post_control($data) {
    $data = preg_replace("/[^a-zA-Zа-яА-Я0-9\-\s]/ui", '', $data);

    return trim($data);
  }

  public function add_group($group_name) {
    if (!empty($group_name)) {
      $group_name = $this->db->to_charset($group_name);
      $column_value = [
        'name' => $group_name,
        'date_cr' => NULL,
        'date_up'=> NULL
      ];
      $data = $this->db->insert('sp_groups', $column_value);
    } else {
      $data = ['err' => 'Значение группы не может быть пустым!'];
    }
    return $data;
  }

  public function del_group($group_id) {
    if (!empty($group_id)) {
      $column_value = [
        'id' => $group_id
      ];
      $result = $this->db->delete('sp_groups', $column_value);
    } else {
      $result = ['err' => 'Data is incorrect'];
    }

    return $result;
  }

  public function upd_group($group_value) {
    if (!empty($group_value)) {
        foreach ($group_value as $key => $value) {
          $column_value[] = [
            'id' => $value[0],
            'name' => $this->post_control($value[1]),
            'date_up' => 'NOW()'
          ];
        }
      $result = $this->db->update('sp_groups', $column_value);
    } else {
      $result = ['err' => 'Data is incorrect'];
    }

    return $result;
  }

  public function post_id($need) {
    $result = [];
    foreach ($_POST as $key => $value) {
      if (strstr($key, $need)) {
        $result[] = $value;
      }
    }

    return $result;
  }

}
