<?php
/**
 *
 */
class Model_Managers extends Model {
  private $db;
  private $db_pdo;
  private $user;

  public function __construct() {
    $this->db = new Database();
    $this->db_pdo = $this->db->get_db();
  }

  public function get_data($value = NULL) {
    $result = [];
    $old_val = $value;
    $result = $this->check_profile($old_val);

    if (!empty($result['gid'])) {
      $value = $result['gid'];
    }

    $groups = $this->db->get_id_name_by_table('sp_groups');

    if (!empty($value) && !empty($groups['id'])) {
      $value = $this->valid_value($value, $groups['id'], ['add', 'notassigned']);
      // 'name'
      if (empty($this->user['view'])) {
        $this->user = $this->db->show_managers($value);
      } else {
        $this->user = $this->db->show_managers($value, 1);
      }
    } else {
      // 'name'
      if (empty($this->user['view'])) {
        $this->user = $this->db->show_managers();
      } else {
        $this->user = $this->db->show_managers(NULL, 1);
      }
    }

    if (!empty($result) && !empty($this->user)) {
      $this->user = array_merge($result, $this->user);
    }

    $user_id = $this->user['id'];
    // check user in table sp_user_stat
    $user_group = $this->check_managers_group($user_id);
    $this->user['user_group'] = $user_group; // use for view checked radio button
    $groups = empty($groups['err']) ? $groups : '';
    $this->user['groups'] = $groups;

    if (!empty($_POST['groups'])) {

      if (empty($_POST['change'])) {
        $this->user['post'] = $this->show_post('radio_');
        }

      if (!empty($_POST['change'])) {
        $this->user['post'] = $this->get_form('change_save-', ['begin', 'end']);

        if (empty($this->user['post'])) {
          $this->user['post'] = $this->get_form('change_del-');
        }
      }

      if (empty($_POST['change'])) {
        $this->user['post_check'] = $this->check_managers_group($this->show_post('radio_', 'uid'));
        // create request to send method add in db
        $send = $this->req_group_user($this->user['post']);
        // save in db change in table of managers

        if (!empty($send)) {
          $this->db->send_arr_request($send);

          foreach ($this->user['post'] as $key => $value) {
            $this->user['user_group'][$key][0] = $value[0];
          }
        }
      } elseif (!empty($_POST['change'])) {
          $stat_group = $this->user['post'];
          $stat_sid = array_keys($stat_group);

          if (empty($stat_group[$stat_sid[0]]['act'])) {
            $this->user['msg'] = $this->db->delete('sp_group_user', ['id' => $stat_sid[0]]);
          } else {
            $update_arr = [[
              'id' => $stat_sid[0],
              'date_begin' => $stat_group[$stat_sid[0]]['date'][0],
              'date_end' => !empty($stat_group[$stat_sid[0]]['date'][1]) ? $stat_group[$stat_sid[0]]['date'][1] : NULL
            ]];
            $this->user['msg'] = $this->db->update('sp_group_user', $update_arr);
          }
        }

      } else {
        $this->user['msg'] = 'no change';
      }

      unset($this->user['prof_all_groups']);
      $result = $this->check_profile($old_val);
      $this->user = array_merge($result, $this->user);

      if (!empty($this->user['uid'])) {
        $this->user['prof_all_mess'] = $this->get_all_chats_messages($this->user['uid']);
      }

    return $this->user;
  }

  public function check_profile($value) {
    $result = [];

    if (strstr($_SERVER['REQUEST_URI'], 'profile/')) {
      $managers_id = $value;
      $result['uid'] = $managers_id;
      $prof = $this->get_group_id($managers_id, 1);

      if (!empty($prof) && empty($prof['err'])) {
        $prof_all = $this->get_group_id($managers_id);
        $result['view'] = 'profile';
        $last_group = sizeof($prof);
        $last_group_id = $last_group - 1;
        $result['prof'] = $prof[$last_group_id];
        $result['prof_groups'] = $prof;
        $result['prof_all_groups'] = $prof_all;
        $result['gid'] = $prof[$last_group_id]['id'];
      }
    } else {
      $result['err'] = 'Этот ID не пренадлежит этой группе.';
    }

    return $result;
  }

  public function check_managers_group($id) {
    $result = [];
    if (!empty($id)) {
      if (is_array($id)) {
        foreach ($id as $key => $value) {
          if (is_array($value) && !empty($value[0])) {
            $result[] = [
              $this->db->get_request('SELECT group_id FROM sp_group_user WHERE user_id = '.$value[0].' AND date_end is NULL')[0],
              $value[0]
            ];
          } else {
            $result[] = [
              $this->db->get_request('SELECT group_id FROM sp_group_user WHERE user_id = '.$value.' AND date_end is NULL')[0],
              $value
            ];
          }
        }
      } else {
        $result[] = [
          $this->db->get_request('SELECT group_id FROM sp_group_user WHERE user_id = '.$id.' AND date_end is NULL')[0],
          $id
        ];
      }
    }
    return sizeof($result) > 0 ? $result : 'Empty';
  }

  public function show_post($need, $save = NULL) {
    $result = [];

    foreach ($_POST as $key => $value) {
      if (strstr($key, $need)) {
        $temp = str_replace($need, '', $key);
        $val = explode('-', $value);
        switch ($save) {
          case 'gid':
            $result[($temp-1)][0] = $val[0];
            break;
          case 'uid':
            $result[($temp-1)][0] = $val[1];
            break;

          default:
            $result[($temp-1)][0] = $val[0];
            if (!empty($val[1])) {
              $result[($temp-1)][1] = $val[1];
            }
            break;
        }
      }
    }

    return $result;
  }

  public function get_form($need, array $form = NULL) {
    $result = [];
    // gre ID
    $need_id = NULL;
    foreach ($_POST as $key => $value) {
        if(strstr($key, $need)) {
          $need_id = str_replace($need, '', $key);
          break;
        }
        if(!empty($need_id)) {
          break;
        }
    }

    if (!empty($need_id)) {
      $result[$need_id] = [];
    }

    // get value date
    if (!empty($need_id) && !empty($form)) {
      $srh_word = str_replace('save-', '', $need);
      $result[$need_id]['act'] = 'save';

      foreach ($_POST as $key => $value) {
        foreach ($form as $val) {
          if ($srh_word.$val.'-'.$need_id == $key) {
            $result[$need_id]['date'][] = $value;
            break;
          }
        }
        if (!empty($result['date'][$need_id]) && sizeof($result['date'][$need_id]) > 1) {
          break;
        }
      }
    }

    return $result;
  }

/*
  public function check_group_user($group_user) {
      $request = [];
      if (is_array($group_user)) {
        foreach ($group_user as $key => $value) {
          $sel = explode('-', $value);
          $request[] = 'SELECT group_id, user_id FROM sp_group_user WHERE group_id = ' . $sel[0] . ' AND user_id = ' . $sel[1];
        }
      }
      return $request;
  }
  */

  public function req_group_user($group_user) {
    $request = [];
    if (is_array($group_user)) {
      $k = 0;
      foreach ($group_user as $key => $value) {
        if (!empty($this->user['post_check'][$k][0]) && (string) $this->user['post_check'][$k][0] === $value[0]) {
          $k++;
          continue;
        }

        if ((string) $this->user['post_check'][$k][0] === $value[0]) {
          $request[] = 'INSERT INTO sp_group_user (group_id, user_id, date_begin, date_end) VALUES (' . $value[0] . ', ' . $value[1] . ', NOW(), NULL)';
        } else {
          $request[] = 'UPDATE sp_group_user SET date_end = NOW() WHERE user_id = '.$value[1];
          $request[] = 'INSERT INTO sp_group_user (group_id, user_id, date_begin, date_end) VALUES (' . $value[0] . ', ' . $value[1] . ', NOW(), NULL)';
        }
        $k++;
      }
    }
    return $request;
  }

  public function get_group_id($group_id, $not_all_group = NULL) {
    $group = $this->db->check_user($group_id, $not_all_group);
    return $group;
  }

  public function get_all_chats_messages($uid) {
    if (!empty($uid)) {
      $uid = $this->db->get_int($uid);
      $sql = [
        'columns' => 'sp_stats.clients, sp_stats.messages, sp_stats.date_w',
        'table' => 'sp_stats, sp_user_stat',
        'where' => 'sp_user_stat.stat_id = sp_stats.id AND sp_user_stat.user_id = ' . $uid,
        'order' => 'sp_stats.date_w DESC'
      ];
      $result = $this->db->select($sql);
    }
    return $result;
  }

}
