<?php
/**
 * Authentification users
 */
class Model_Auth extends Model {
  private $auth;
  private $db;

  public function __construct() {
    $this->auth = new Auth();
  }

  public function get_data($value = null) {
    $result = false;

    if (!empty($_POST['log'])) {
      $result = $this->auth->authUser();
    }

    return $result;
  }
}
