<?php
/**
 *
 */
class Model_Main extends Model {
  private $db;

  public function __construct() {
    $this->db = new Database();
  }

  public function get_data() {
    $req = $this->start_stat();
    if (!empty($req['sql'])) {
      $data = $this->db->get_row_request($req['sql']);
      $data = $this->get_charts_data($data);
      if (!empty($data['datas'])) {
        ksort($data['datas']);
      }
//      if (!empty($data['groups'])) {
        $data['groups'] = $this->db->get_id_name_by_table('sp_groups');
//      }

      if (!empty($data['groups']['name'])) {
        $data['groups']['name'] = $this->get_q_arr($data['groups']['name']);
      }
    }

    return $data;
  }

  public function start_stat() {
    $req = [];
    $begin = $this->month_begin_end()['begin'];
    $end = $this->month_begin_end()['end'];

/*
SELECT user.id as uid, userstat.stat_id as sid, usergroup.group_id as gid,
 user.name as name, stat.clients as chat, stat.messages as mes,
 usergroup.date_begin as begin, usergroup.date_end as end, stat.date_w as workday,
 SUM(stat.clients) as chats, SUM(stat.messages) as mess, COUNT(stat.date_w) as workdays
FROM sp_users user
LEFT JOIN sp_user_stat userstat ON user.id = userstat.user_id
LEFT JOIN sp_stats stat ON stat.id = userstat.stat_id
LEFT JOIN sp_group_user usergroup ON user.id = usergroup.user_id
WHERE stat.date_w >= usergroup.date_begin AND (usergroup.date_end >= stat.date_w OR usergroup.date_end IS NULL) AND
 ( stat.date_w >= '2019-06-01' AND stat.date_w <= '2019-06-30' )
GROUP BY name ORDER BY chats DESC
*/
    $req['sql'] = 'SELECT ' .
                  'usergroup.group_id as gid, ' .
                  'stat.clients as chat, ' .
                  'stat.messages as mes, ' .
                  'usergroup.date_begin as begin, ' .
                  'usergroup.date_end as end, ' .
                  'stat.date_w as workday, ' .
                  'SUM(stat.clients) as chats, ' .
                  'SUM(stat.messages) as mess, ' .
                  'COUNT(stat.date_w) as workdays ' .
                  'FROM sp_users user ' .
                  'LEFT JOIN sp_user_stat userstat ON user.id = userstat.user_id ' .
                  'LEFT JOIN sp_stats stat ON stat.id = userstat.stat_id ' .
                  'LEFT JOIN sp_group_user usergroup ON user.id = usergroup.user_id ' .
                  'WHERE ' .
                  'stat.date_w >= usergroup.date_begin AND ' .
                  '(usergroup.date_end >= stat.date_w OR usergroup.date_end IS NULL) AND ( ' .
                    "stat.date_w >= '$begin' AND stat.date_w <= '$end' ".
                  ') ' .
                  'GROUP BY ' .
                  'gid, workday ' .
                  'ORDER BY ' .
                  'gid ASC, workday ASC';

    return $req;
  }

  public function month_begin_end($year_month = NULL) {
    if (!empty($year_month)) {
      $temp = explode("/", $year_month);
      $year = ((int) $temp[0] > 2018 && (int) $temp[0] < 2050) ? (int) $temp[0]: '';
      $month = ((int) $temp[1] > 0 && (int) $temp[0] < 13) ? (int) $temp[1]: '';
    }

    if (empty($year) || empty($month)) {
      $year = date("Y");
      $month = date("m");
      }

    switch ($month) {
      case '04':
      case '06':
      case '09':
      case '11':
        $end = 30;
        break;

      case '02':
        $end = ($year % 4 == 0 || ($year % 100 == 0 && $year % 400 == 0)) ? 29 : 28;
        break;

      default:
        $end = 31;
        break;
    }

    $result['begin'] = date("$year-m-01");
    $result['end'] = date("Y-m-" . $end);

    return $result;
  }

  public function get_charts_data($data) {
    $result = [];
    if (!empty($data) && is_array($data)) {
      foreach ($data as $key => $value) {
        $result['datas'][$value['workday']][$value['gid']] = [$value['chats'], $value['mess']];
      }
      $result['days'] = array_keys($result['datas']);
      sort($result['days']);
      $result['days'] = $this->get_q_arr($result['days']);
    }
    return $result;
  }

  public function get_q_arr($arr) {
    foreach ($arr as $value) {
      $temp[] = $this->db->valid_data($value);
    }
    return $temp;
  }

}
