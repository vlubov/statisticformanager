<?php
/**
 *
 */
class Model_Import extends Model {
  private $db;

  public function __construct() {
    $this->db = new Database();
  }

  public function get_data($value = NULL) {
    $result = $this->file_content();

    if (empty($result)) {
      $result = [];
    }

    if (!empty($value)) {
      $result = array_merge($result, $this->del($value));
    }

    $file = new Fileman;
    $result = array_merge($result, $file->view_list_files());

    return $result;
  }

  public function upload() {
    $result['path'] = '';
    $result['name'] = '';
    $result['msg'] = '';
    $result['err'] = '';
    $dir = './' . DIR_UPL . '/';

    if (is_dir($dir)) {
      if (!empty($_FILES['sendfile']['name'])) {
        $result['dir'] = ' Dir is exist ';
        $date = basename($_FILES['sendfile']['name'], EXT_TXT);
        $path = $dir . $_FILES['sendfile']['name'];
        $result['path'] = $path;
        $result['date'] = $date;

        if (!$this->check_date($date)) {
          $result['msg'] = 'Неверное имя файла: <b>' . $date . '</b>';
          $result['err'] = 'Ошибка: ';
          return $result;
        }

        $result['name'] = ' ' . $date . EXT_TXT;

        if (!is_file($path) && move_uploaded_file($_FILES['sendfile']['tmp_name'], $path)) {
          $result['msg'] = "Успех: > Файл " . $result['name'] . " корректен и был успешно загружен.";
        } elseif (is_file($path)) {
          $result['msg'] = " > Такой файл уже загружен!";
          $result['err'] = 'Ошибка: ';
        } else {
          $result['err'] = " > Файл не загружен! Попробуйте повторить загрузку";
        }
      }
    } else {
      $result['msg'] = 'Создайте директорию в корне проекта: ' . $dir;
      $result['err'] = 'Ошибка: ';
    }

    return !empty($result) ? $result : ' NOT!';
  }

  public function file_content() {
    $msg = [];
    if (!empty($_POST['filesubmit'])) {
      $uploud_file = $this->upload();
      if (empty($uploud_file['err']) && !empty($uploud_file['path'])) {
        $msg['msg'] = $uploud_file['msg'];
        $file_array = file($uploud_file['path']);
        // create 1 array in file
        $file_arr = $this->get_content($file_array);
        // create 2 array to SQL
        $sql = $this->set_add_sql($file_arr, $uploud_file['date']);
        // create send data to DB
        $send_db = $this->send_to_db($sql);

      } else {
        $msg['err'] = $uploud_file['err'] . $uploud_file['name'] . $uploud_file['msg'];
      }
    } elseif (!empty($_POST['hidesend'])) {
      $msg['err'] = 'Произошла ошибка, файл не был отправлен!';
    }

    return isset($msg) ? $msg : '';
  }

  public function code_charset($value) {
    $coding = [
      'CP1251',
      'UTF-8',
      'ASCII'
    ];

    $code = mb_detect_encoding($value, 'auto', TRUE);

    if ($code !== 'UTF-8') {
      $value = mb_convert_encoding($value, 'UTF-8', 'CP1251');
      }

    return $value;
  }

  public function get_content($file_arr) {
    $result = [];
    if (is_array($file_arr)) {
      $i = 0;
      foreach ($file_arr as $key => $value) {
        if ($i === 0) {
          $i++;
          continue;
        }
        $str = str_replace("\t", "", trim($value));
        $res_str = explode(" ", trim($str));
        $temp = [];
        $j = 0;
        foreach ($res_str as $key_res => $val_res) {
          if(!empty($val_res)) {
            if ($j > 3) {
              $temp[$j - 1] = $temp[$j - 1].' '.$this->code_charset($val_res);
              break;
            } else {
              $temp[$j] = $this->code_charset($val_res);
            }
            $j++;
          }
        }
        $res_str = $temp;
        $result[] = $res_str;
        $i++;
      }
    }

    return $result;
  }

  public function set_add_sql($arr, $date) {
    $sql = [];
    $i = 0;

    foreach ($arr as $key => $value) {
      // sql to add in sp_stats
      $sql['sp_stat_ins'][$i]['prepare'] = 'INSERT INTO sp_stats (clients, messages, date_w) VALUES (?, ?, ?)';
      $sql['sp_stat_ins'][$i]['execute'] = [$value[1], $value[2], $date];
      // sql test user_id in sp_users
      $sql['sp_user_sel'][$i]['sql'] = 'SELECT id FROM sp_users WHERE uid = '.$value[0];
      // sql to add in sp_users
      $sql['sp_user_ins'][$i]['prepare'] = 'INSERT INTO sp_users (uid, name, date_up) VALUES (?, ?, ?)';
      $sql['sp_user_ins'][$i]['execute'] = [$value[0], "$value[3]", NULL];
      $i++;
    }

    return !empty($sql) ? $sql : 'Request SQL is NULL';
  }

  public function send_to_db($sql) {
    $result = [];
    $result['box'] = 'empty!';
    $pdo = $this->db->get_db();
    $pdo_obj = $this->db;
    if(!empty($pdo) && is_array($sql)) {
      unset($result);
      $result['box'] = '<hr><hr>';
      $result['box'] .= '<div class="container small">';

      // add to db all statistics
      $i = 0;
      foreach ($sql['sp_stat_ins'] as $key => $value) {
        $stmt = $pdo->prepare($value['prepare']);
        $pdo->beginTransaction();
        $stmt->execute($value['execute']);
        $result['stat_ins'][$i] = $pdo->lastInsertId();
        $pdo->commit();
        $i++;
      }

      // check user prepare add to db user
      $i = 0;
      foreach ($sql['sp_user_sel'] as $key => $value) {
        $result['user_sel'][$i] = sizeof($pdo_obj->get_request($value['sql'])) > 0 ? $pdo_obj->get_request($value['sql']) : NULL;
        $i++;
      }

      // if check user and user not found add to db user
      $i = 0;
      foreach ($sql['sp_user_ins'] as $key => $value) {
        if (!empty($result['user_sel'][$i])) {
          $result['user_ins'][$i] = $result['user_sel'][$i][0];
          $i++;
          continue;
        }

        $stmt = $pdo->prepare($value['prepare']);
        $pdo->beginTransaction();
        $stmt->execute($value['execute']);
        $result['user_ins'][$i] = $pdo->lastInsertId();
        $pdo->commit();
        $i++;
      }

      if (is_array($result['stat_ins']) && is_array($result['user_ins'])) {
        for ($i = 0; $i < sizeof($result['stat_ins']); $i++) {
          $result['user_stat'][$i] = $pdo_obj->get_request(
            'INSERT INTO sp_user_stat (stat_id, user_id)
             VALUES ('.$result['stat_ins'][$i].', '.$result['user_ins'][$i].')'
          );
        }
      }


      $result['box'] .= '</div>';
    }
    return $result;
  }

  public function check_date($date) {
    $result = FALSE;
    $arr = explode('-', $date);
// Test::eh($date, 1);
// Test::eh(sizeof($arr), 1);
    if (sizeof($arr) === 3 && strlen($date) === 10) {
      foreach ($arr as $key => $value) {
        $arr[$key] = (int) $value;
      }

      if (!empty($arr[0]) && !empty($arr[1]) && !empty($arr[2]) &&
          $arr[0] > 2018 && $arr[0] < 2050 &&
          checkdate($arr[1], $arr[2], $arr[0])
      ) {
        $result = TRUE;
      }
    }

    return $result;
  }

  public function del($name) {
    $result = [];
    $file_name = DIR_UPL . '/' . $name;
    if (file_exists($file_name)) {
      $result['del_msg'] = 'Файл существует';
      if (unlink($file_name)) {
        $result['del_msg'] = 'Файл [<b>' . $name . '</b>] успешно удален.';
      } else {
        $result['del_err'] = 'Файл [<b>' . $name . '</b>] не удален!';
      }
    } elseif ($name === 'all') {
      // $result['del_msg'] = 'Удалить все файлы!';
      $files = new Fileman;
      $all_files = $files->view_list_files();
      $msg = '';
      $err = '';

      foreach ($all_files['file_names'] as $key => $value) {
        if (unlink(DIR_UPL . '/' . $value)) {
          $msg .= 'Файлы ' . $value . ' удалён! ';
        } else {
          $err .= 'Файлы ' . $value . ' не удалён! ';
        }
      }

      $result['del_msg'] = $msg;
      $result['del_err'] = $err;
    } else {
      $result['del_err'] = 'Файла [<b>' . $name . '</b>] не существует.';
    }

    return $result;
  }

}
