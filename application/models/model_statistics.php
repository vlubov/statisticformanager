<?php
/**
 *
 */
class Model_Statistics extends Model {
  private $db;
  private $db_pdo;
  private $act_date;

  public function __construct() {
    $this->db = new Database();
    $this->db_pdo = $this->db->get_db();
  }

  public function get_data($value = NULL) {
    if (!empty($_POST)) {
      $result['send'] = $_POST;
    }

    $result['today'] = date("Y-m-d");
    $result['groups'] = $this->db->get_id_name_by_table('sp_groups');

    if (empty($value) || $value === 'all' || $value === 'notassigned') {
      $result['group'] = [
        'id' => $value,
        'name' => (empty($value) || $value === 'all') ? 'Все группы' : 'Нет группы'
      ];
    }

    if (!empty($value) && !empty($result['groups']['id'])) {
      $value = $this->valid_value($value, $result['groups']['id'], ['add', 'notassigned']);
      $result['managers'] = $this->db->show_managers($value);

      if (!empty($this->act_date)) {
        $result['req'] = $this->trigger($this->act_date, $result['send']['group']);
        }

      $id = $this->db->get_int($value);

      if (!empty($id)) {
        $result['group'] = [
          'id' => $id,
          'name' => $result['groups']['name'][array_flip($result['groups']['id'])[$id]]
        ];
      }
    } else {
      $result['managers'] = $this->db->show_managers();

      if (!empty($this->act_date) && !empty($result['send']['group'])) {
        $result['req'] = $this->trigger($this->act_date, $result['send']['group']);
        }
    }

    if (!empty($result['send']['group'])) {
      $result['group'] = [
        'id' => $result['send']['group'],
        'name' => ($result['send']['group'] !== 'all' && $result['send']['group'] !== 'notassigned') ?
        $result['groups']['name'][array_flip($result['groups']['id'])[$result['send']['group']]] :
        (($result['send']['group'] === 'all') ? 'Все группы' : 'Нет группы')
      ];
    }

    if (!empty($result['req']['sql'])) {
      $result['exec'] = $this->db->get_row_request($result['req']['sql'][0]);
      $req['sql'] = $result['req']['sql'][1];
      $data = $this->db->get_row_request($req['sql']);
      $data = $this->get_charts_data($data);
      $result = array_merge($data, $result);

      if (!empty($result['uid']['name'])) {
        $result['managers']['name'] = array_keys(array_flip($result['uid']['name']));
        $result['managers']['id'] = array_keys($result['uid']['name']);
      }
    }

    $result['dates'] = $this->getDataWeek();

    if (!empty($_POST['dates']['setDateNext']) && (!empty($_POST['gid']) || !empty($_POST['gidid']))) {
      $result['dates']['selectBegin'] = $result['dates']['dateBeginNext'];
      $result['dates']['selectEnd'] = $result['dates']['dateEndNext'];
    } elseif (!empty($_POST['dates']['setDateNext']) && (!empty($_POST['gid']) || !empty($_POST['gidid']))) {
      $result['dates']['selectBegin'] = $result['dates']['dateBeginLast'];
      $result['dates']['selectEnd'] = $result['dates']['dateEndLast'];
    } else {
      $result['dates']['selectBegin'] = $result['dates']['dateBegin'];
      $result['dates']['selectEnd'] = $result['dates']['dateEnd'];
    }

    if (!empty($_POST['gid']) || !empty($_POST['gidid'])) {
      $gid = !empty($_POST['gid']) ? $_POST['gid'] : $_POST['gidid'];
      $result['dates'] = array_merge($this->getDataManagers('week', [$result['dates']['selectBegin'], $result['dates']['selectEnd'], $gid]),$result['dates']);
    }

    if (!empty($result['dates']['sql'][0])) {
      // Test::pre($result['dates']['sql'][0]);
      $result['weekStats'] = $this->db->get_row_request($result['dates']['sql'][0]);
      $result['dataTable'] = $this->reorgonizeData($result['weekStats']);
    }

    return $result;
  }

  public function reorgonizeData($arWeekData) {
    $newData = [];

    if (!empty($arWeekData)) {
      foreach ($arWeekData as $key => $value) {
        $newData[$value['name']][$value['workday']] = [$value['chats'], $value['mess']];
      }

      foreach ($newData as $key => $val) {
        ksort($newData[$key]);
      }

      ksort($newData);
    }

    return $newData;
  }

  public function get_list_group() {
    return $this->db->get_id_name_by_table('sp_groups');
  }

  public function get_charts_data($data) {
    $result = [];
    $result['users'] = [];

    if (!empty($data)) {
      foreach ($data as $key => $value) {
        $result['datas'][$value['workday']][$value['uid']] = [$value['chats'], $value['mess']];
        $result['users'] = array_flip(array_merge(array_keys($result['datas'][$value['workday']]), array_flip($result['users'])));
        $result['uid']['name'][$value['uid']] = $value['name'];
      }

      ksort($result['datas']);
      $result['users'] = array_keys($result['users']);
      $result['days'] = array_keys($result['datas']);
      $result['days'] = $this->get_q_arr($result['days']);
    }

    return $result;
  }

  public function convertDateTime($dates) {
    $date = [];
    foreach ($dates as $value) {
      $date[] = strtotime($value);
    }
    return $date;
  }

  public function convertTimeDate($times) {
    $date = [];
    foreach ($times as $value) {
      $date[] = date("Y-m-d", $value);
    }

    return $date;
  }

  public function get_q_arr($arr) {
    foreach ($arr as $value) {
      $temp[] = $this->db->valid_data($value);
    }

    return $temp;
  }

  public function set_date($value) {
    if (!empty($value)) {
      $this->act_date = $value;
    }
  }

  public function get_act_date() {
    return $this->act_date;
  }

  /*
  * get dates for create select in week period
  */
  public function getDataWeek() {
    $date = time();

    if (!empty($_POST['setDateLast']) || !empty($_POST['setDateNext']) ) {
      $convertDate = !empty($_POST['setDateLast']) ? $_POST['setDateLast'] : $_POST['setDateNext'];
      $date = $this->convertDateTime([$convertDate]);
      $date = $date[0];
    }

    $dates = date("w", $date);
    $week = 3600 * 24 * 7;
    // get week day number, then get begin week and end week data
    // if 1 - is begin week &&  end week = today + 6 day
    // if 0 - is end week   &&  begin week = today - 6 day
    // left = 1; right = 7 $dates ~ today;
    $today = !empty($dates) ? $dates : 7;

    $day = 3600 * 24;
    $beginDate = ($today - 1) * $day;
    $endDate = (7 - $today) * $day;
    $result['dateToday'] = date("Y-m-d", $date);
    $result['lastWeek'] = date("Y-m-d", $date - $week);
    $result['nextWeek'] = date("Y-m-d", $date + $week);
    $result['leftStep'] = $beginDate;
    $result['rightStep'] = $endDate;
    $result['dateBegin'] = date("Y-m-d", $date - $beginDate);
    $result['dateEnd'] = date("Y-m-d", $date + $endDate);

    $result['dateBeginLast'] = date("Y-m-d", $date - $beginDate - $week);
    $result['dateEndLast'] = date("Y-m-d", $date + $endDate - $week);
    $result['dateBeginNext'] = date("Y-m-d", $date - $beginDate + $week);
    $result['dateEndNext'] = date("Y-m-d", $date + $endDate + $week);

    return $result;
  }

  /*
  * $period = 'week'
  * $dates = ['date_begin, 'date_end', 'group_id']
  */
  public function getDataManagers($period, $datas) {
    switch ($period) {
      case 'week':
        $group = ($datas[2] != 'all' && $datas[2] != 'notassigned') ? ("usergroup.group_id = $datas[2] AND ") : '';

        $req['sql'][0] = 'SELECT ' .
                'user.id as uid, ' .
                'userstat.stat_id as sid, ' .
                'usergroup.group_id as gid, ' .
                'user.name as name, ' .
                'stat.clients as chat, ' .
                'stat.messages as mes, ' .
                'usergroup.date_begin as begin, ' .
                'usergroup.date_end as end, ' .
                'stat.date_w as workday, ' .
                'SUM(stat.clients) as chats, ' .
                'SUM(stat.messages) as mess, ' .
                'COUNT(stat.date_w) as workdays ' .
              'FROM sp_users user ' .
              'LEFT JOIN sp_user_stat userstat ON user.id = userstat.user_id ' .
              'LEFT JOIN sp_stats stat ON stat.id = userstat.stat_id ' .
              'LEFT JOIN sp_group_user usergroup ON user.id = usergroup.user_id ' .
              'WHERE ' .
                $group .
                'stat.date_w >= usergroup.date_begin AND ' .
                '(usergroup.date_end >= stat.date_w OR usergroup.date_end IS NULL) AND ( ' .
                  "stat.date_w >= '$datas[0]' AND stat.date_w <= '$datas[1]' ".
                ') ' .
              'GROUP BY ' .
                'name, workday ' .
              'ORDER BY ' .
                'chats DESC';
        break;

      default:
        // code...
        break;
    }

    $result = $req;

    return $result;
  }

  public function trigger($value, $arr_id) {
    $req = [];
    if (!empty($arr_id) && is_array($arr_id)) {
      $arr_id = implode(', ', $arr_id);
    }

    $begin = date("Y-m-d", time() - 3600 * 24);
    $end = date("Y-m-d", time());
    $date_begin = date("Y-m-d", time() - 3600 * 24);
    $date_end = ' &mdash; ' . date("Y-m-d", time());
    if (!empty($_POST['begin'])) {
      $begin = $_POST['begin'];
      $date_begin = $_POST['begin'];
    }
    if (!empty($_POST['end'])) {
      $end = $_POST['end'];
      $date_end = ' &mdash; ' . $_POST['end'];
    }

    switch ($value) {

      case 'yesterday':
        $req['sql'] = 'SELECT sp_user_stat.user_id, sp_users.name, SUM(sp_stats.clients) AS clients, SUM(sp_stats.messages) AS messages, COUNT(sp_stats.date_w) AS days FROM ' .
        'sp_stats, sp_user_stat, sp_users WHERE ' .
        'sp_user_stat.user_id = sp_users.id AND ' .
        'sp_user_stat.stat_id = sp_stats.id AND ' .
        'sp_stats.date_w >= (CURDATE() - 1) AND ' .
        'sp_stats.date_w < CURDATE() AND ' .
        'sp_user_stat.user_id IN (' . $arr_id . ') ' .
        'GROUP BY sp_users.name ' .
        'ORDER BY clients DESC';
        $req['text'] = 'Вчера ' . (date("Y-m-d", time() - 3600*24) ) ;
        break;

      case 'week':
        $begin = $_POST['dateBegin'];
        $end = $_POST['dateEnd'];
        $group = ($arr_id != 'all' && $arr_id != 'notassigned') ? ("usergroup.group_id = $arr_id AND ") : '';

        $req['sql'][0] = 'SELECT ' .
                'user.id as uid, ' .
                'userstat.stat_id as sid, ' .
                'usergroup.group_id as gid, ' .
                'user.name as name, ' .
                'stat.clients as chat, ' .
                'stat.messages as mes, ' .
                'usergroup.date_begin as begin, ' .
                'usergroup.date_end as end, ' .
                'stat.date_w as workday, ' .
                'SUM(stat.clients) as chats, ' .
                'SUM(stat.messages) as mess, ' .
                'COUNT(stat.date_w) as workdays ' .
              'FROM sp_users user ' .
              'LEFT JOIN sp_user_stat userstat ON user.id = userstat.user_id ' .
              'LEFT JOIN sp_stats stat ON stat.id = userstat.stat_id ' .
              'LEFT JOIN sp_group_user usergroup ON user.id = usergroup.user_id ' .
              'WHERE ' .
                $group .
                'stat.date_w >= usergroup.date_begin AND ' .
                '(usergroup.date_end >= stat.date_w OR usergroup.date_end IS NULL) AND ( ' .
                  "stat.date_w >= '$begin' AND stat.date_w <= '$end' ".
                ') ' .
              'GROUP BY ' .
                'name, workday ' .
              'ORDER BY ' .
                'chats DESC';
      /*
        $date_begin = date("Y-m-d", time() - 3600*24*7);
        $date_end = ' &mdash; ' . date("Y-m-d", time());
        $req['sql'] = 'SELECT sp_user_stat.user_id, sp_users.name, SUM(sp_stats.clients) AS clients, SUM(sp_stats.messages) AS messages, COUNT(sp_stats.date_w) AS days FROM ' .
        'sp_stats, sp_user_stat, sp_users WHERE ' .
        'sp_user_stat.user_id = sp_users.id AND ' .
        'sp_user_stat.stat_id = sp_stats.id AND ' .
        'sp_stats.date_w >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) AND ' . // ' . $begin . ' AND ' .
        // 'sp_stats.date_w < ' . $end . ' AND ' .
        'sp_user_stat.user_id IN (' . $arr_id . ') ' .
        'ORDER BY clients DESC';
        */
        $req['text'] = '<b class="h5"> Период: ' . $date_begin . $date_end . '</b>';
        break;

      case 'month':
        $date_begin = date("Y-m-d", time() - 3600*24*30);
        $date_end = ' &mdash; ' . date("Y-m-d", time());
        $req['sql'] = 'SELECT sp_user_stat.user_id, sp_users.name, SUM(sp_stats.clients) AS clients, SUM(sp_stats.messages) AS messages, COUNT(sp_stats.date_w) AS days FROM ' .
        'sp_stats, sp_user_stat, sp_users WHERE ' .
        'sp_user_stat.user_id = sp_users.id AND ' .
        'sp_user_stat.stat_id = sp_stats.id AND ' .
        'sp_stats.date_w >= DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY) AND ' .
        'sp_user_stat.user_id IN (' . $arr_id . ') ' .
        'GROUP BY sp_users.name ' .
        'ORDER BY clients DESC';
        $req['text'] = '<b class="h5"> Период: ' . $date_begin . $date_end . '</b>';
        break;

      case 'period':
          $add = (!empty($end) && $end !== 'CURDATE()') ? 'sp_stats.date_w <= \'' . $end . '\') AND ' : '';
          $add2 = (!empty($end) && $end !== 'CURDATE()') ? 'sp_group_user.date_end <= \'' . $end . '\') AND ' : '';
          $group = ($arr_id != 'all' && $arr_id != 'notassigned') ? ("usergroup.group_id = $arr_id AND ") : '';

          $req['sql'][0] = 'SELECT ' .
                  'user.id as uid, ' .
                  'userstat.stat_id as sid, ' .
                  'usergroup.group_id as gid, ' .
                  'user.name as name, ' .
                  'stat.clients as chat, ' .
                  'stat.messages as mes, ' .
                  'usergroup.date_begin as begin, ' .
                  'usergroup.date_end as end, ' .
                  'stat.date_w as workday, ' .
                  'SUM(stat.clients) as chats, ' .
                  'SUM(stat.messages) as mess, ' .
                  'COUNT(stat.date_w) as workdays ' .
                'FROM sp_users user ' .
                'LEFT JOIN sp_user_stat userstat ON user.id = userstat.user_id ' .
                'LEFT JOIN sp_stats stat ON stat.id = userstat.stat_id ' .
                'LEFT JOIN sp_group_user usergroup ON user.id = usergroup.user_id ' .
                'WHERE ' .
                  $group .
                  'stat.date_w >= usergroup.date_begin AND ' .
                  '(usergroup.date_end >= stat.date_w OR usergroup.date_end IS NULL) AND ( ' .
                    "stat.date_w >= '$begin' AND stat.date_w <= '$end' ".
                  ') ' .
                'GROUP BY ' .
                  'name ' .
                'ORDER BY ' .
                  'chats DESC';

          $req['sql'][1] = 'SELECT ' .
                  'user.id as uid, ' .
                  'userstat.stat_id as sid, ' .
                  'usergroup.group_id as gid, ' .
                  'user.name as name, ' .
                  'stat.clients as chat, ' .
                  'stat.messages as mes, ' .
                  'usergroup.date_begin as begin, ' .
                  'usergroup.date_end as end, ' .
                  'stat.date_w as workday, ' .
                  'SUM(stat.clients) as chats, ' .
                  'SUM(stat.messages) as mess, ' .
                  'COUNT(stat.date_w) as workdays ' .
                'FROM sp_users user ' .
                'LEFT JOIN sp_user_stat userstat ON user.id = userstat.user_id ' .
                'LEFT JOIN sp_stats stat ON stat.id = userstat.stat_id ' .
                'LEFT JOIN sp_group_user usergroup ON user.id = usergroup.user_id ' .
                'WHERE ' .
                  $group .
                  'stat.date_w >= usergroup.date_begin AND ' .
                  '(usergroup.date_end >= stat.date_w OR usergroup.date_end IS NULL) AND ( ' .
                    "stat.date_w >= '$begin' AND stat.date_w <= '$end' ".
                  ') ' .
                'GROUP BY ' .
                  'name, workday ' .
                'ORDER BY ' .
                  'chats DESC';

          $req['text'] = '<b class="h5"> Период: ' . $date_begin . $date_end . '</b>';
        break;

      default:
          $req['err'] = 'some trouble to create sql request';
        break;
    }

    return $req;
  }
}
