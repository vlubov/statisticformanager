<?php
/**
 * TEST class including function FOR TEST
 */
class Test {

  public function pre($arr, $param = 'p') {
    if (!empty($arr)) {
      switch ($param) {
        case 'd':
          echo '<pre>';
          var_dump($arr);
          echo '</pre>';
          break;
        case 'e':
          echo '<pre>';
          var_export($arr);
          echo '</pre>';
          break;
        case 'et':
          echo '<pre>';
          var_export($arr, true);
          echo '</pre>';
          break;

        default:
        // p - some print_r
          echo '<pre>';
          print_r($arr);
          echo '</pre>';
          break;
      }
    }
    return;
  }

  public function eh($text, $br = NULL) {
    if (!empty($text)) {
      if (!empty($br)) {
        $br = '<br>';
      } else {
        $br = '';
      }
      echo $text.$br;
    }
    return;
  }

}
