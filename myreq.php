<?php
require_once 'application/config.php';
require_once 'application/core/autolouder.php';


$base = str_replace('myreq.php', '', $_SERVER['SCRIPT_NAME']);
  $auth = new Auth;
  $data['genPass'] = $auth->genPass();
  $data['genName'] = $auth->genName();
  $data['passHash'] = $auth->hashPass($data['genPass']);
  $data['passVerHash'] = $auth->verifPass($data['genPass'], $data['passHash']);
if (!empty($_POST['pin'])) {
  $sendData = [
    'log' => $_POST['login'],
    'pas' => $_POST['pass'],
    'pin' => $_POST['pin'],
    'crtUsr' => $_POST['crt_usr']
  ];
  $msg = $auth->createUser($sendData);
}
?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <script src="<?php echo $base; ?>js/jquery.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/bootstrap.min.css">
  <script type="text/javascript" src="<?php echo $base; ?>js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/style.css">
  <title>Add users</title>
</head>
<body>
<div class="container">
  <br>
  <div class="row">
    <div class="col h2 pl-4">Создать пользователя</div>
    <div class="col h4 text-right pt-2 pr-4"><?php echo date('d-m-Y H:i:s'); ?></div>
  </div>
  <hr>
  <div class="row">
    <div class="col text-right">
  <form action="myreq.php" method="post">
    <div class="row m-2">
      <div class="col-3 mt-1">Логин:</div>
      <div class="col-7">
        <input type="text" placeholder="Input login" name="login"
        value="<?php echo !empty($data['genName']) ? $data['genName'] : ''; ?>" class="form-control">
      </div>
    </div>
    <div class="row m-2">
      <div class="col-3 mt-1">Пароль:</div>
      <div class="col-7">
        <input type="text" placeholder="Input password" name="pass"
        value="<?php echo !empty($data['genPass']) ? $data['genPass'] : ''; ?>" class="form-control">
      </div>
    </div>
    <div class="row m-2">
      <div class="col-3 mt-1">Пин:</div>
      <div class="col-7"><input type="text" placeholder="Input PIN code" name="pin" class="form-control"></div>
    </div>
    <div class="row m-2">
      <div class="col-10">
        <button class="btn btn-outline-warning btn-sm" type="button" onclick="location.reload();return false;">
          Generate
        </button>
        <button class="btn btn-outline-success btn-sm" type="submit" name="crt_usr" value="create">
          Create User!
        </button>
      </div>
    </div>
  </form>
    </div>
    <div class="col">
      <div class="row m-2">
        <div class="col pr-5">
      Для создания пользователя: введите латинницей <b>логин</b> и <b>пароль</b>, и подтвердите действие числовым
      <b>пин кодом</b>.<br><br>Если пин код отсутствует или не верен &mdash; пользователь создан не будет.
        </div>
      </div>
    </div>
  </div>
  <br><hr><br><br>
<?php if (!empty($_POST['pin']) && $_POST['pin'] == PIN_CODE) { ?>
  <div class="container">
    <div class="row">
      <div class="col alert alert-info"><?php echo !empty($msg['msg']) ? $msg['msg'] : (!empty($msg['err']) ? $msg['err'] : 'Nothing'); ?></div>
    </div>
    <div class="row">
      <div class="col-5 alert-link"><br>SAVE THIS DATA:</div>
    </div>
    <div class="row">
      <div class="col">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-2 text-right">login : </div>
      <div class="col-3 alert-link"><?php echo $_POST['login']; ?></div>
    </div>
    <div class="row">
      <div class="col-2 text-right">password : </div>
      <div class="col-3 alert-link"><?php echo $_POST['pass']; ?></div>
    </div>
  </div>
  <br><hr><br><br>
<?php } elseif (!empty($_POST['pin'])) { ?>
    <div class="container">
      <div class="row">
        <div class="col alert alert-danger">Для создания пользователя нужно ввести верный Пин код!</div>
      </div>
    </div>
    <br><hr><br><br>
<?php } ?>
</div>
</body>
</html>
